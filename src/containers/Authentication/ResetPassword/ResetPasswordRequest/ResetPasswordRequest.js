/**
 * Created by urbanmarovt on 15/04/16.
 */

import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import {Panel} from 'react-bootstrap';
import {toastr} from 'react-redux-toastr';

import {ResetPasswordRequestForm} from 'components';

import {requestResetPassword} from 'redux/modules/utils/resetPassword';
import stringsOptions from './strings';


@connect(
  state => ({
    chosenLanguage: state.global.language.chosenLanguage,
  }),
  dispatch => bindActionCreators({...routerActions, requestResetPassword}, dispatch)
)
export default class ResetPasswordRequest extends Component {

  static propTypes = {
    requestResetPassword: PropTypes.func
  };

  handleSubmit = (data) => {
    const {requestResetPassword, chosenLanguage, pushState} = this.props;
    const strings = stringsOptions[chosenLanguage];

    requestResetPassword(data).then((res) => {
      if (res.error) {
        toastr.error(strings.emailNotFound);
      } else {
        toastr.success(strings.checkYourEmail);
        pushState(null, '/');
      }
    });
  };

  render() {

    const {chosenLanguage} = this.props;
    const strings = stringsOptions[chosenLanguage];

    return (
      <Panel className="reset-password-container">
        <h3 className="mb24"> {strings.resetPassword} </h3>
        <ResetPasswordRequestForm onSubmit={this.handleSubmit} />
      </Panel>
    );
  }
}
