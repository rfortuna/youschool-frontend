import superagent from 'superagent';
import config from '../config';

const methods = ['get', 'post', 'put', 'patch', 'del'];

function formatUrl(path) {
  const adjustedPath = path[0] !== '/' ? '/' + path : path;
  if (__SERVER__) {
    // Prepend host and port of the API server to the path.

    return 'http://' + config.apiHost + ':' + config.apiPort + adjustedPath;

  }
//   Prepend `/api` to relative URL, to proxy to API server.
  return '/api' + adjustedPath;
}

/*
 * This silly underscore is here to avoid a mysterious "ReferenceError: ApiClient is not defined" error.
 * See Issue #14. https://github.com/erikras/react-redux-universal-hot-example/issues/14
 *
 * Remove it at your own risk.
 */
class _ApiClient {
  constructor(req) {

    methods.forEach((method) =>
      this[method] = (path, { params, data, formData, token, multiPart } = {}) => new Promise((resolve, reject) => {
        const request = superagent[method](formatUrl(path));

        if (params) {
          request.query(params);
        }

        request.set('Authorization', `Bearer ${token}`);

        if (__SERVER__ && req.get('cookie')) {
          request.set('cookie', req.get('cookie'));
        }

        if (formData) {
          request.type('form');
          Object.keys(formData).forEach((key) => {
            request.send(`${key}=${formData[key]}`);
          });
        }

        // sending files
        if (multiPart) {
          let formData = new FormData();
          formData.append('file', multiPart.file);
          Object.keys(multiPart).forEach((key) => {
            if (key !== 'file') {
              console.log(key);
              formData.append(key, JSON.stringify(multiPart[key]));
            }
          });
          request.send(formData);
        }

        if (data) {
          request.send(data);
        }

        request.end((err, res = {}) => err ? reject(res.body || err) : resolve({headers: res.headers, body: res.body}));
      }));
  }
}

const ApiClient = _ApiClient;

export default ApiClient;
