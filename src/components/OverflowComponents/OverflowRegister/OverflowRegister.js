/**
 * Created by rokfortuna on 4/26/16.
 */

import React, {Component, PropTypes} from 'react';
import {Button, Modal, Row, Col, Glyphicon, FormControl,
  FormGroup, ControlLabel, HelpBlock} from 'react-bootstrap';

import {connect} from 'react-redux';
import connectData from 'helpers/connectData';
import {reduxForm, getValues} from 'redux-form';
import {bindActionCreators} from 'redux';
import * as routerActions from 'redux-router';
import {CustomSelect} from 'components';
import { hideRegisterModal } from 'redux/modules/views/navigation';
import _ from 'lodash';
import {toastr} from 'react-redux-toastr';

import {createAccount} from 'redux/modules/authentication';
import {verifyEmailRequest} from 'redux/modules/utils/verifyEmail';
import { validateEmail, validateMobilePhone, isInteger } from 'utils/validation';
import {translateFormErrors} from 'utils/localization';
import {load as loadProfileData} from 'redux/modules/global/profile';
import {refactorCountries} from 'utils/refactor';
import {trackEvent as trackEventMixpanel} from 'redux/modules/global/mixpanel';

import strings from './strings';

const validate = (values) => {

  const errors = {};

  if (!values.firstName) {
    errors.firstName = 'required';
  }

  if (!values.lastName) {
    errors.lastName = 'required';
  }

  if (!values.email) {
    errors.email = 'required';
  }
  else if (!validateEmail(values.email)) {
    errors.email = 'invalidEmail';
    //Invalid email address.
  }

  if (!values.password) {
    errors.password = 'required';
  } else if (values.password.length < 8) {
    errors.password = 'passTooShort';
  }

  if (!values.passwordCheck) {
    errors.passwordCheck = 'required';
  } else if (values.passwordCheck !== values.password) {
    errors.passwordCheck = 'passNotMatch'
  }

  if (values.mobileNumber && !validateMobilePhone(values.mobileNumber)) {
    errors.mobileNumber = 'invalidMobileNumber'
  }

  if (values.currentGrade && !isInteger(values.currentGrade)) {
    errors.currentGrade = 'invalidInteger'
  }

  if (values.targetGrade && !isInteger(values.targetGrade)) {
    errors.targetGrade = 'invalidInteger'
  }

  return errors;

};

@connect(
  state => ({
    chosenLanguage: state.global.language.chosenLanguage,
    registerForm: state.form.RegisterForm,
    loggingIn: state.authentication.loggingIn,
    countries: state.models.countries.list.data
  }),
  dispatch => bindActionCreators(
    {...routerActions,
      hideRegisterModal,
      createAccount,
      verifyEmailRequest,
      loadProfileData,
      trackEventMixpanel
    }, dispatch)
)
@reduxForm({
  form: 'RegisterForm',
  fields: ['firstName', 'lastName', 'email', 'password', 'passwordCheck', 'city', 'country', 'mobileNumber', 'school', 'schoolClass', 'currentGrade', 'targetGrade'],
  validate: validate
})
export default class OverflowRegister extends Component {

  static propTypes = {
    chosenLanguage: PropTypes.string,
    handleSubmit: PropTypes.func,
    visible: PropTypes.bool,
    hideRegisterModal: PropTypes.func,
    loadProfileData: PropTypes.func,
    countries: PropTypes.array
  };

  handleSubmit = () => {
    const {registerForm, createAccount, verifyEmail, hideRegisterModal, verifyEmailRequest, loadProfileData, chosenLanguage, trackEventMixpanel} = this.props;

    const data = getValues(registerForm);

    const stringsChosen = strings[chosenLanguage];

    createAccount(data).then((res) => {
      if (res.error) {
        toastr.error(res.error.error);
        throw res.error.error;
      } else {
        toastr.success(stringsChosen.toastrs.registerSuccess);
        trackEventMixpanel("register", {email: data.email});
        loadProfileData();
        return verifyEmailRequest(res.result.body.id);
      }
    }).then((res) => {
      hideRegisterModal();
    }).catch((e) => {
      console.log(e);
    });

  }

  render() {
    const {visible, handleSubmit, handleCancel, chosenLanguage,
       video, hideRegisterModal, fields: {firstName, lastName, email, password, passwordCheck,
         city, country, mobileNumber, school, schoolClass, currentGrade,
         targetGrade}, errors, loggingIn, countries} = this.props;

    const stringsChosen = strings[chosenLanguage];
    const translatedErrors = translateFormErrors(errors, stringsChosen.errors);

    return (
      <Modal show={visible} onHide={hideRegisterModal} bsSize="large" className="modal">
        <form>
          <Modal.Header closeButton>
            <Modal.Title>{stringsChosen.createAccount}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Col xs={12} sm={6}>
                <FormGroup validationState={ errors.firstName && firstName.touched ? 'error' : null}>
                  <FormControl type="text" placeholder={stringsChosen.placeholders.firstName} {...firstName} />
                  {firstName.touched && errors.firstName && <HelpBlock>{translatedErrors.firstName}</HelpBlock>}
                </FormGroup>
              </Col>
              <Col xs={12} sm={6}>
                <FormGroup validationState={ errors.lastName && lastName.touched ? 'error' : null}>
                  <FormControl type="text" placeholder={stringsChosen.placeholders.lastName} {...lastName} />
                  {lastName.touched && errors.lastName && <HelpBlock>{translatedErrors.lastName}</HelpBlock>}
                </FormGroup>
              </Col>
              <Col xs={12} sm={6}>
                <FormGroup validationState={ errors.email && email.touched ? 'error' : null}>
                  <FormControl type="text" placeholder={stringsChosen.placeholders.email} {...email} />
                  {email.touched && errors.email && <HelpBlock>{translatedErrors.email}</HelpBlock>}
                </FormGroup>
              </Col>
              <Col xs={12} sm={6}>
                <FormGroup validationState={ errors.mobileNumber && mobileNumber.touched ? 'error' : null}>
                  <FormControl type="text" placeholder={stringsChosen.placeholders.mobileNumber} {...mobileNumber} />
                  {mobileNumber.touched && errors.mobileNumber && <HelpBlock>{translatedErrors.mobileNumber}</HelpBlock>}
                </FormGroup>
              </Col>
              <Col xs={12} sm={6}>
                <FormGroup validationState={ errors.password && password.touched ? 'error' : null}>
                  <FormControl type="password" placeholder={stringsChosen.placeholders.password} {...password} />
                  {password.touched && errors.password && <HelpBlock>{translatedErrors.password}</HelpBlock>}
                </FormGroup>
              </Col>
              <Col xs={12} sm={6}>
                <FormGroup validationState={ errors.passwordCheck && passwordCheck.touched ? 'error' : null}>
                  <FormControl type="password" placeholder={stringsChosen.placeholders.passwordCheck} {...passwordCheck} />
                  {passwordCheck.touched && errors.passwordCheck && <HelpBlock>{translatedErrors.passwordCheck}</HelpBlock>}
                </FormGroup>
              </Col>
              <Col xs={12} sm={6}>
                <FormGroup>
                  <FormControl type="text" placeholder={stringsChosen.placeholders.school} {...school} />
                </FormGroup>
              </Col>
              <Col xs={12} sm={6}>
                <FormGroup>
                  <FormControl type="text" placeholder={stringsChosen.placeholders.schoolClass} {...schoolClass} />
                </FormGroup>
              </Col>
              <Col xs={12} sm={6}>
                <FormGroup validationState={ errors.currentGrade && currentGrade.touched ? 'error' : null}>
                  <FormControl type="text" placeholder={stringsChosen.placeholders.currentGrade} {...currentGrade} />
                  {currentGrade.touched && errors.currentGrade && <HelpBlock>{translatedErrors.currentGrade}</HelpBlock>}
                </FormGroup>
              </Col>
              <Col xs={12} sm={6}>
                <FormGroup validationState={ errors.targetGrade && targetGrade.touched ? 'error' : null}>
                  <FormControl type="text" placeholder={stringsChosen.placeholders.targetGrade} {...targetGrade} />
                  {targetGrade.touched && errors.targetGrade && <HelpBlock>{translatedErrors.targetGrade}</HelpBlock>}
                </FormGroup>
              </Col>
              <Col xs={12} sm={6}>
                <FormGroup>
                  <FormControl type="text" placeholder={stringsChosen.placeholders.city} {...city} />
                </FormGroup>
              </Col>
              <Col xs={12} sm={6}>
                <FormGroup>
                  <CustomSelect {...country} data={refactorCountries(countries)} placeholder={stringsChosen.placeholders.country}/>
                </FormGroup>
              </Col>
            </Row>

          </Modal.Body>
          <Modal.Footer>
            <Row>
              <Col xs={12} sm={6}>
                <Button disabled={_.keys(errors).length !== 0} className="pull-left full-width button-primary" onClick={this.handleSubmit}>
                  {(!loggingIn &&
                  <span>{stringsChosen.createAccount}</span>) ||
                    <span><i className="fa fa-spinner fa-spin"></i></span>}
                </Button>
              </Col>
            </Row>
          </Modal.Footer>
        </form>
      </Modal>
    );
  }
}
