/**
 * Created by urbanmarovt on 26/04/16.
 */

export default {
  en: {
    swishPayment: 'Swish payment',
    price: 'Price',
    credits: 'Credits',
    mobileNumber: 'Mobile number',
    mobileNumberPlaceholder: 'Enter your Swish account mobile number ...',
    actions: {
      requestPayment: 'Request payment'
    },
    errors: {
      required: 'Required',
      invalidMobileNumber: 'Mobile number is not valid'
    },
    toastrs: {
      paymentRequestSuccess: 'Check your swish mobile application to continue the payment!',
      paymentRequestError: 'Unregistered mobile number!'
    }
  },
  sv: {
    swishPayment: 'Swish payment',
    price: 'Price',
    credits: 'Credits',
    mobileNumber: 'Mobile number',
    mobileNumberPlaceholder: 'Enter your Swish account mobile number ...',
    actions: {
      requestPayment: 'Request payment'
    },
    errors: {
      required: 'Required',
      invalidMobileNumber: 'Mobile number is not valid'
    },
    toastrs: {
      paymentRequestSuccess: 'Check your swish mobile application to continue the payment!',
      paymentRequestError: 'Unregistered mobile number!'
    }
  }
};
