/**
 * Created by rokfortuna on 4/26/16.
 */

export default {
  en: {
    createAccount: 'Create account',
    closeModal: 'Close',
    yes: 'Yes',
    no: 'No',
    placeholders: {
      firstName: 'First name',
      lastName: 'Last name',
      email: 'Email',
      mobileNumber: 'Mobile number',
      password: 'Password',
      passwordCheck: 'Reenter password',
      school: 'School name',
      schoolClass: 'Class name',
      currentGrade: 'Current maths grade',
      targetGrade: 'Target maths grade',
      city: 'Home city',
      country: 'Home country'
    },
    createAccount: 'Create account',
    youAlreadyHaveAccount: 'You already have an account?',
    login: 'Log In',
    errors: {
      required: 'Required.',
      invalidEmail: 'Email address is invalid.',
      passTooShort: 'Password is too short (8 characters at least).',
      passNotMatch: 'Passwords does not match.',
      invalidMobileNumber: 'Mobile number is invalid.',
      invalidInteger: 'Input should be a number.'
    },
    toastrs: {
      registerSuccess: 'You have successfully created an account. Check your email to verify your account!'
    }
  },
  sv: {
    createAccount: 'Create account',
    closeModal: 'Close',
    yes: 'Yes',
    no: 'No',
    placeholders: {
      firstName: 'First name',
      lastName: 'Last name',
      email: 'Email',
      mobileNumber: 'Mobile number',
      password: 'Password',
      passwordCheck: 'Reenter password',
      school: 'School name',
      schoolClass: 'Class name',
      currentGrade: 'Current maths grade',
      targetGrade: 'Target maths grade',
      city: 'Home city',
      country: 'Home country'
    },
    createAccount: 'Create account',
    youAlreadyHaveAccount: 'You already have an account?',
    login: 'Log In',
    errors: {
      required: 'Required.',
      invalidEmail: 'Email address is invalid.',
      passTooShort: 'Password is too short (8 characters at least).',
      passNotMatch: 'Passwords does not match.',
      invalidMobileNumber: 'Mobile number is invalid.',
      invalidInteger: 'Input should be a number.'
    },
    toastrs: {
      registerSuccess: 'You have successfully created an account. Check your email to verify your account!'
    }
  }
};
