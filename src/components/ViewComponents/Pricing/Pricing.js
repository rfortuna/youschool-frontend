import React, { Component, PropTypes } from 'react';
import * as routerActions from 'redux-router';
import {connect} from 'react-redux';
import {reduxForm, reset as resetForm} from 'redux-form';
import {bindActionCreators} from 'redux';
import {Grid, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router';

import stringsOptions from './strings.js';


@connect(
  state => ({
    plans: state.models.plans.list.data,
    chosenLanguage: state.global.language.chosenLanguage,
  }),
  dispatch => bindActionCreators(
    {...routerActions,
    }, dispatch)
)
export default class Pricing extends Component {

  static propTypes = {
    chosenLanguage: PropTypes.string,
    plans: PropTypes.array
  };

  onOfferClick = (offer) => {
    console.log(offer);
  }

  render() {

    const {plans, chosenLanguage} = this.props;
    const strings = stringsOptions[chosenLanguage];
    // console.log(plans);

    return (
      <div className="pricing-component text-center">
        <h3>{strings.pricing}</h3>
        <div className="mt16">{strings.pricingSubtitle}</div>
        <div className="section-title mt32">
          <hr/> <span className="text-uppercase"> {strings.solutionVideos} </span> <hr/>
        </div>
        <Grid className="mt40 mb64">
          <Row className="offers-container">
            {plans
              .filter((plan) => plan.localization == chosenLanguage)
              .map((plan) =>
              <Col xs={12} sm={6} md={4} key={plan.id}>
                <div className="offer-container text-center">
                  <div className="offer-container-inner pt16 pr16 pl16">
                    <div className="text-uppercase text-size-sm">{plan.name}</div>
                    <div style={{margin: '0 auto'}}>
                      <div className="price pr16">
                        <span className="currency">{plan.currency}</span>
                        <span className="amount">{plan.amount}</span>
                        <span> </span>
                      </div>
                    </div>
                    <div className="description mt8 text-size-xs">
                      {plan.description}
                    </div>
                    {plan.mostPopular &&
                      <div className="most-popular-tag text-center"> {strings.mostPopular} </div>
                    }
                  </div>
                  <Link to={`/payment/${plan.id}`} className="text-decoration-none">
                    <div className={`button-buy ${plan.mostPopular ? 'most-popular' : ''}`}>
                      {strings.buyNow}
                    </div>
                  </Link>
                </div>
              </Col>
            )}
          </Row>
        </Grid>

      </div>
    );
  }
}
