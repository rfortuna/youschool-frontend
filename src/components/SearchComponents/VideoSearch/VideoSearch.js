/**
 * Created by urbanmarovt on 4/24/16.
 */

import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {FormControl, FormGroup, Button, ControlLabel, Form} from 'react-bootstrap';
import {Link} from 'react-router';
import _ from 'lodash';

import {SimpleSelectInline} from 'components';
import multiStrings from './strings';

const validate = (values) => {

  const errors = {};


  return errors;

};

@reduxForm({
  form: 'VideoSearch',
  fields: ['book', 'exercise'],
  validate: validate
})
export default class VideoSearch extends Component {

  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    fields: PropTypes.object,
    errors: PropTypes.object,
    allBooks: PropTypes.array,
    chosenLanguage: PropTypes.string
  };

  render() {
    const styles = require('./VideoSearch.scss');
    const {fields: {book, exercise}, allBooks, errors, chosenLanguage} = this.props;
    const strings = multiStrings[chosenLanguage];

    return (
      <Form inline onSubmit={this.props.handleSubmit}>
        <FormGroup className={styles.paddingRight}>
          <SimpleSelectInline placeholder={strings.bookPlaceholder} data={allBooks} {...book} />
        </FormGroup>
        <FormGroup className={styles.paddingRight}>
          <FormControl type="text" placeholder={strings.exercisePlaceholder} {...exercise} />
        </FormGroup>
        <Button disabled={_.keys(errors).length !== 0} type="submit">Search</Button>
      </Form>
    );
  }
}
