/**
 * Created by rokfortuna on 4/26/16.
 */


import React, {Component, PropTypes} from 'react';
import {Button, Modal, Row, Col, Glyphicon} from 'react-bootstrap';

import strings from './strings';

export default class OverflowRateVideo extends Component {

  static propTypes = {
    chosenLanguage: PropTypes.string,
    handleCancel: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func,
    visible: PropTypes.bool
  };

  render() {
    const {visible, handleSubmit, handleCancel, chosenLanguage, video} = this.props;
    const stringsChosen = strings[chosenLanguage];

    return (
      <Modal show={visible} onHide={handleCancel} bsSize="small">
        <Modal.Header closeButton>
          <Modal.Title>{stringsChosen.wasVideoHelpful}</Modal.Title>
        </Modal.Header>
        <Modal.Footer>
          <Button onClick={handleSubmit(true)}>
            {stringsChosen.yes}
          </Button>
          <Button onClick={handleSubmit(true)}>
            {stringsChosen.no}
          </Button>
          <Button onClick={handleCancel}>{stringsChosen.closeModal}</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

/*
* <Row>
 <Col xs={6}>
 <Button onClick={handleSubmit(true)}>
 <Glyphicon glyph="thumbs-up" />
 </Button>
 </Col>
 <Col xs={6}>
 <Button onClick={handleSubmit(false)}>
 <Glyphicon glyph="thumbs-down" />
 </Button>
 </Col>
 </Row>*/
