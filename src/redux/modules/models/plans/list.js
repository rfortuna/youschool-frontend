/**
 * Created by urbanmarovt on 25/04/16.
 */

const LOAD = 'youschool/plans/LOAD';
const LOAD_SUCCESS = 'youschool/plans/LOAD_SUCCESS';
const LOAD_FAIL = 'youschool/plans/LOAD_FAIL';

const initialState = {
  loaded: false,
  loading: false,
  error: {},
  data: []
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        data: [],
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.body,
        error: null
      };
    case LOAD_FAIL:
      return {
        ...initialState,
        error: action.error
      };
    default:
      return state;
  }
}

// put globalState as parameters
export function load() {

  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token) => client.get(`/plans`, { token: token})
  };
}
