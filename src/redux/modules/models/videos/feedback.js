/**
 * Created by rokfortuna on 4/26/16.
 */

const HAS_USER_RATED = 'youschool/video/feedback/HAS_USER_RATED';
const HAS_USER_RATED_SUCCESS = 'youschool/video/feedback/HAS_USER_RATED_SUCCESS';
const HAS_USER_RATED_FAIL = 'youschool/video/feedback/HAS_USER_RATED_FAIL';

const RATE = 'youschool/video/feedback/RATE';
const RATE_SUCCESS = 'youschool/video/feedback/RATE_SUCCESS';
const RATE_FAIL = 'youschool/video/feedback/RATE_FAIL';

const REPORT_ERROR = 'youschool/video/feedback/RATE';
const REPORT_ERROR_SUCCESS = 'youschool/video/feedback/REPORT_ERROR_SUCCESS';
const REPORT_ERROR_FAIL = 'youschool/video/feedback/REPORT_ERROR_FAIL';

const CLEAR_STATE = 'youschool/video/feedback/CLEAR_STATE';

const initialState = {
  rate: {
    hasRated: false,
    liked: null
  }
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case HAS_USER_RATED:
    return state;
    case HAS_USER_RATED_SUCCESS:
      return {
        ...state,
        rate: {
          ...action.result.body
        }
      };
    case HAS_USER_RATED_FAIL:
      return {
        ...state,
        rate: {
          hasRated: false,
          liked: null
        }
      };
    case RATE:
      return state;
    case RATE_SUCCESS:
      console.log(action.result.body);
      return {
        ...state,
        rate: {
          hasRated: true,
          liked: action.result.body.rate
        }
      };
    case RATE_FAIL:
      return {
        ...state,
        rate: {
          hasRated: false,
          liked: null
        }
      };
    case REPORT_ERROR:
    case REPORT_ERROR_SUCCESS:
    case REPORT_ERROR_FAIL:
      return state;
    case CLEAR_STATE:
      return initialState;
    default:
      return state;
  }
}

export function hasRated(videoId) {
  return {
    types: [HAS_USER_RATED, HAS_USER_RATED_SUCCESS, HAS_USER_RATED_FAIL],
    promise: (client, token) => client.get(`/feedback/rated/${videoId}`, {token: token})
  }
}

export function rate(videoId, liked) {
  return {
    types: [RATE, RATE_SUCCESS, RATE_FAIL],
    promise: (client, token) => client.post(`/feedback/rate/${videoId}`, {token: token, formData: {liked: liked}})
  }
}

export function reportError(videoId, description) {
  return {
    types: [REPORT_ERROR, REPORT_ERROR_SUCCESS, REPORT_ERROR_FAIL],
    promise: (client, token) => client.post(`/feedback/reportError/${videoId}`, {token: token, formData: {description: description}})
  }
}

export function clear() {
  return {
    type: CLEAR_STATE
  }
}