import React, { Component, PropTypes } from 'react';
import DocumentMeta from 'react-document-meta';
import config from '../../config';
import ReduxToastr from 'react-redux-toastr';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import { FormControl, Col, Grid } from 'react-bootstrap';
import * as routerActions from 'redux-router';

import {TopMenu} from 'components';
import {load as loadProfile, isLoaded as isProfileLoaded, clear as clearProfileState} from 'redux/modules/global/profile';
import { isLoaded as isTokenLoaded, logout} from 'redux/modules/authentication';
import {init as initMixpanel, identify as identifyMixpanel} from 'redux/modules/global/mixpanel';
import {initIntercom} from 'utils/intercom';
import {load as loadCountries} from 'redux/modules/models/countries/list';

function fetchDataDeferred(getState, dispatch) {
  let promises = [];

  if (!isProfileLoaded(getState()) && isTokenLoaded(getState())) {
    promises.push(dispatch(loadProfile()));
  }

  promises.push(dispatch(loadCountries()));

  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    token: state.authentication.token,
    profile: state.global.profile,
    mixpanel: state.global.mixpanel,
    language: state.global.language
  }),
  dispatch => bindActionCreators(
    {...routerActions, initMixpanel, identifyMixpanel, logout, clearProfileState}, dispatch)
)
export default class App extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired,
    profile: PropTypes.object,
    mixpanel: PropTypes.object,
    initMixpanel: PropTypes.func,
    identifyMixpanel: PropTypes.func,
    logout: PropTypes.func,
    clearProfileState: PropTypes.func,
    language: PropTypes.object,
    token: PropTypes.string
  };

  static contextTypes = {
    store: PropTypes.object.isRequired
  };

  componentDidMount() {
    const {mixpanel, profile, initMixpanel, identifyMixpanel} = this.props;
    if (!mixpanel.mixpanel) {
      initMixpanel();
    }
    if (profile.id) {

      identifyMixpanel(profile.id);

      initIntercom(profile);
    }
  }



  logout = () => {

    this.props.pushState(null, '/');
    window.intercomSettigs = null;

    setTimeout(() => {
      this.props.logout();
      this.props.clearProfileState();
    }, 100);
  }


  render() {
    const styles = require('./App.scss');
    const {profile, language, pushState} = this.props;

    return (
      <div>
        {profile.id &&
          <script>
            {

          }
          </script>
        }
        <div className={styles.app}>
          <DocumentMeta {...config.app}/>
          <div>

            <TopMenu
              logout={this.logout}
              profile={profile}
              pushState={pushState}
            />

            <div className={`${styles.appContent}`}>
              {this.props.children}
            </div>
            <div className="footer text-center">
              <br/>
              <div>
                <strong>Copyright</strong> YouSchool &copy; 2014-2016
              </div>
            </div>
          </div>
          <ReduxToastr
            timeOut={2000}
            newestOnTop={false}
            position="top-right"/>
        </div>
      </div>
    );
  }
}
