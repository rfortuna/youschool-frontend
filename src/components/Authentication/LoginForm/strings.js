/**
 * Created by rokfortuna on 4/22/16.
 */

export default {
  sv : {
    emailPlaceholder: 'swe_Enter email address',
    passwordPlaceholder: 'swe_Enter password',
    login: 'swe_Login',
    forgotPassword: 'swe_Forgot password?',
    dontHaveAccount: 'swe_Do not have an account?',
    register: 'swe_Register'
  },
  en: {
    emailPlaceholder: 'Enter email address',
    passwordPlaceholder: 'Enter password',
    login: 'Login',
    forgotPassword: 'Forgot password?',
    dontHaveAccount: 'Do not have an account?',
    register: 'Register'
  }
};