/**
 * Created by rokfortuna on 4/14/16.
 */

import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {FormControl, FormGroup, Button, ControlLabel, HelpBlock} from 'react-bootstrap';
import {connect} from 'react-redux';
import { validateEmail } from 'utils/validation';
import {translateFormErrors} from 'utils/localization';

import _ from 'lodash';

import stringsOptions from './strings';

const validate = (values) => {

  const errors = {};

  if (!values.email) {
    errors.email = 'required';
  }else if (!validateEmail(values.email)) {
    errors.email = 'invalidEmail';
    //Invalid email address.
  }

  return errors;
};

@connect(
  state => ({
    chosenLanguage: state.global.language.chosenLanguage,
    requesting: state.utils.resetPassword.requestingResetPassword
  })
)
@reduxForm({
  form: 'ResetPasswordRequestForm',
  fields: ['email'],
  validate: validate
})
export default class ResetPasswordRequestForm extends Component {

  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    fields: PropTypes.object,
    errors: PropTypes.object
  };

  render() {
    const {fields: {email}, errors, handleSubmit, chosenLanguage, requesting } = this.props;
    const strings = stringsOptions[chosenLanguage];

    const translatedErrors = translateFormErrors(errors, strings.errors);

    return (
      <form onSubmit={handleSubmit} className="reset-password-form">
        <FormGroup validationState={ errors.email && email.touched ? 'error' : null}>
          <ControlLabel>{strings.email}</ControlLabel>
          <FormControl type="text" placeholder={strings.enterEmailAddress} {...email} />
          {email.touched && errors.email && <HelpBlock>{translatedErrors.email}</HelpBlock>}
        </FormGroup>
        <Button disabled={_.keys(errors).length !== 0} className="button-primary" type="submit">
          {(!requesting &&
          <span>{strings.resetPassword}</span>) ||
            <span><i className="fa fa-spinner fa-spin"></i></span>}
        </Button>
      </form>
    );
  }
}
