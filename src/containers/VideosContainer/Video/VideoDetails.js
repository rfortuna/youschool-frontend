/**
 * Created by urbanmarovt on 24/04/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import {reduxForm, reset as resetForm} from 'redux-form';
import {Link} from 'react-router';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import cookie from 'react-cookie';
import {Row, Col, Button, Glyphicon, FormGroup, FormControl} from 'react-bootstrap';
import {load as loadUser} from 'redux/modules/models/users/details';
import Video from 'react-html5video';
import _ from 'lodash';

import multiStrings from './strings';
import {translateFormErrors} from 'utils/localization';

import {Spinner} from 'components';

import {
  OverflowPurchaseVideo,
  OverflowBuyCredits,
  OverflowReportError,
  VideoActionIcon
} from 'components';

import {
  load as loadVideo,
  loadUrl as loadVideoUrl,
  purchase as purchaseVideo,
  clear as clearDetailsState
} from 'redux/modules/models/videos/details';

import {
  reportError,
  clear as clearFeedbackState
} from 'redux/modules/models/videos/feedback';

import {
  showPurchaseModal,
  hidePurchaseModal,
  showBuyCreditsModal,
  hideBuyCreditsModal,
  showRateModal,
  hideRateModal,
  showReportModal,
  hideReportModal
} from 'redux/modules/views/video';

import {load as loadBooks} from 'redux/modules/models/books/list';
import {updateCredits as updateProfileCredits} from 'redux/modules/global/profile';
import {load as loadComments} from 'redux/modules/models/comments/list';
import {create as createComment} from 'redux/modules/models/comments/new';
import {load as loadConsecutiveVideos} from 'redux/modules/models/videos/consecutive';
import {clearSelectOptions as clearVideoSelectOptions} from 'redux/modules/models/videos/list';
import { showLoginModal } from 'redux/modules/views/navigation';
import {trackEvent as trackEventMixpanel} from 'redux/modules/global/mixpanel';

import {toastr} from 'react-redux-toastr';

import {refactorBooks} from 'utils/refactor';

const validate = (values) => {

  const errors = {};

  if (!values.comment) {
    errors.comment = 'required';
  }

  return errors;

};

function fetchDataDeferred(getState, dispatch) {
  const promises = [];

  const videoId = getState().router.params.videoId;

  promises.push(dispatch(loadBooks()));
  promises.push(dispatch(loadVideo(videoId)));
  promises.push(dispatch(loadVideoUrl(videoId)));
  promises.push(dispatch(loadComments(videoId)));
  promises.push(dispatch(loadConsecutiveVideos(videoId)));

  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    video: state.models.videos.details.data,
    videoUrl: state.models.videos.details.url,
    chosenLanguage: state.global.language.chosenLanguage,
    allBooks: state.models.books.list.data,
    purchaseModalVisible: state.views.video.purchaseModalVisible,
    buyCreditsModalVisible: state.views.video.buyCreditsModalVisible,
    rateModalVisible: state.views.video.rateModalVisible,
    reportModalVisible: state.views.video.reportModalVisible,
    profile: state.global.profile,
    feedback: state.models.videos.feedback,
    comments: state.models.comments.list.data,
    creatingComment: state.models.comments.new.data,
    consecutive: state.models.videos.consecutive.data
  }),
  dispatch => bindActionCreators({
    ...routerActions,
    loadVideoUrl,
    purchaseVideo,
    updateProfileCredits,
    reportError,
    createComment,
    loadComments,
    resetForm,
    //clear state
    clearFeedbackState,
    clearDetailsState,
    //modals
    showPurchaseModal,
    hidePurchaseModal,
    showBuyCreditsModal,
    hideBuyCreditsModal,
    showRateModal,
    hideRateModal,
    showReportModal,
    hideReportModal,
    clearVideoSelectOptions,
    showLoginModal,
    trackEventMixpanel
  }, dispatch)
)
@reduxForm({
  form: 'CommentForm',
  fields: ['comment'],
  validate: validate
})
export default class VideoDetails extends Component {

  static propTypes = {
    fields: PropTypes.object,
    allBooks: PropTypes.array,
    video: PropTypes.object,
    videoUrl: PropTypes.string,
    chosenLanguage: PropTypes.string,
    loadVideoUrl: PropTypes.func,
    purchaseVideo: PropTypes.func,
    updateProfileCredits: PropTypes.func,
    profile: PropTypes.object,
    hasUserRatedVideo: PropTypes.bool,
    createComment: PropTypes.func,
    loadComments: PropTypes.func,
    resetForm: PropTypes.func,
    consecutive: PropTypes.object,
//    checkIfRatedVideo: PropTypes.func,
    clearFeedbackState: PropTypes.func,
    clearDetailsState: PropTypes.func,
    clearSelectOptions: PropTypes.func,
    trackEventMixpanel: PropTypes.func,

    //modals
    showPurchaseModal: PropTypes.func,
    closePurchaseModal: PropTypes.func,
    showBuyCreditsModal: PropTypes.func,
    closeBuyCreditsModal: PropTypes.func,
    showRateModal: PropTypes.func,
    hideRateModal: PropTypes.func,
    showReportModal: PropTypes.func,
    hideReportModal: PropTypes.func,
    purchaseModalVisible: PropTypes.bool,
    buyCreditsModalVisible: PropTypes.bool,
    rateModalVisible: PropTypes.bool,
    reportModalVisible: PropTypes.bool,

  };

  state = {
    loading: true
  }

  componentDidMount() {
    const {clearVideoSelectOptions, video, profile, trackEventMixpanel} = this.props;
    trackEventMixpanel('user_clicked_video', {videoId: video.id, videoName: video.name, email: profile.email ? profile.email : 'NOT_LOGGED_IN'});
    let self = this;
    setTimeout(() => {
      self.setState({loading: false});
      clearVideoSelectOptions();
    }, 500);
  }

  componentWillUnmount() {
    const {clearFeedbackState, clearDetailsState} = this.props;
    clearFeedbackState();
    clearDetailsState();
  }

  handleHelpRequest = () => {
    console.log("HELP REQUEST");
  };

  handlePurchaseVideoSubmit = () => {
    const {purchaseVideo, updateProfileCredits,
      hidePurchaseModal, params: {videoId},
      loadProfile, chosenLanguage, profile, video, trackEventMixpanel} = this.props;

    const strings = multiStrings[chosenLanguage];

    purchaseVideo(videoId).then(res => {
      if (res && res.result) {
        updateProfileCredits(res.result.body.User.credits);
        hidePurchaseModal();
        toastr.success(strings.toastrs.reportPurchaseSuccess);
        trackEventMixpanel('user_purchased_video', {videoId: videoId, videoName: video.name, email: profile.email});
      } else {
        hidePurchaseModal();
        toastr.success(strings.toastrs.reportPurchaseFailure);
      }
    });
  };

  handleBuyCreditsSubmit = () => {
    const {pushState} = this.props;

    pushState(null, '/buyCredits');
  };

  handleOnVideoClick = () => {
    const {loadVideoUrl, params: {videoId}, showPurchaseModal,
    showBuyCreditsModal, profile, pushState, video, showLoginModal, trackEventMixpanel} = this.props;

    trackEventMixpanel('user_wanted_to_watch_video', {videoId: videoId, videoName: video.name, email: profile.email ? profile.email : 'NOT_LOGGED_IN'});

    if (!profile.id) {
      showLoginModal();
      return;
    }

    loadVideoUrl(videoId).then((res) => {
      if (res && res.error) {
        if (profile.credits < video.credits) {
          console.log('credits modal');
          showBuyCreditsModal();
        } else {
          console.log('credits modal');
          showPurchaseModal();
        }
      } else {
        console.log("SHOW VIDEO!");
      }
    });
  };

  handleOnVideoEnd = () => {
    console.log("ON VIDEO END!");

  };

  // handleRateSubmit = (liked) => {
  //   const {video, rateVideo, hideRateModal} = this.props;
  //   return () => {
  //     rateVideo(video.id, liked);
  //     hideRateModal();
  //   };
  // };

  handleReportErrorSubmit = (description) => {
    const {video, hideReportModal, reportError, chosenLanguage} = this.props;

    const strings = multiStrings[chosenLanguage];

    return () => {
      reportError(video.id, description.value);
      hideReportModal();
      toastr.success(strings.toasts.reportErrorSuccess);
    };
  };

  changeVideo = (id) => {
    const {pushState, clearDetailsState, clearFeedbackState} = this.props;

    clearDetailsState();
    clearFeedbackState();

    pushState(null, `/videos/${id}`);

  }

  onVideoPause = (time) => {
    const {profile, video, trackEventMixpanel} = this.props;
    trackEventMixpanel('user_paused_video', {videoId: video.id, videoName: video.name, email: profile.email , pausedAt: this.refs.videoPlayer.state.currentTime, duration: this.refs.videoPlayer.state.duration });
  }

  prevVideo = () => {
    console.log("Link to prev video.")
  };

  nextVideo = () => {
    console.log("Link to next video.")
  };

  handleCommentSubmit = () => {
    const {createComment, loadComments, resetForm, fields: {comment}, video} = this.props;

    createComment(comment.value, video.id).then((res) => {
      if (res.error) {
        toastr.error(res.error);
      } else {
        loadComments(video.id);
        resetForm('CommentForm');
      }
    });
  }

  render() {
    const {video, videoUrl, chosenLanguage, comments, fields: {comment}, errors,
      allBooks, purchaseModalVisible, buyCreditsModalVisible, profile,
      hidePurchaseModal, hideBuyCreditsModal, consecutive,
      hideRateModal, rateModalVisible,
      reportModalVisible, hideReportModal, showReportModal} = this.props;

    const strings = multiStrings[chosenLanguage];

    return (
      <div className="video-details container padding-top-container">
        <OverflowPurchaseVideo visible={purchaseModalVisible} handleCancel={hidePurchaseModal} handleSubmit={this.handlePurchaseVideoSubmit} video={video} />
        <OverflowBuyCredits visible={buyCreditsModalVisible} handleCancel={hideBuyCreditsModal} handleSubmit={this.handleBuyCreditsSubmit}/>
        <OverflowReportError visible={reportModalVisible} handleCancel={hideReportModal} reportError={this.handleReportErrorSubmit} video={video} />

        <Row className="text-center">
          {this.state.loading && <Spinner />}
        </Row>
        {!this.state.loading &&
          <div>
            <Row className="pt48 pb48">
              <Col xs={12} sm={7} className="video-container">
                {!videoUrl &&
                  <div className="video-image-container" onClick={this.handleOnVideoClick}>
                    <span className="video-image-overlay"/>
                    <img src={video.imageUrl ? video.imageUrl : '/images/video-placeholder.png'} alt="Placeholder" className={`video-placeholder clickable video-image`} />
                  </div>
                }
                {videoUrl &&
                <Video controls
                       onEnded={this.handleOnVideoEnd}
                       onPause={this.onVideoPause}
                       ref="videoPlayer"
                       >
                  <source src={videoUrl} type="video/mp4" />
                </Video>
                }
                <Row className="pt8 pb8">
                  <Col xs={1} className="text-center" disabled={true}>
                    {(consecutive.prevVideo &&
                    <i onClick={() => this.changeVideo(consecutive.prevVideo.id)} className="fa fa-angle-left text-size-mlg text-center clickable"></i>) ||
                    <i className="fa fa-angle-left text-size-mlg disabled color-gray-light"></i>
                    }
                  </Col>
                  <Col xs={10} className="video-info text-center">
                    <p className="margin0 text-uppercase text-size-sm">
                      {video.name}
                    </p>
                    <p className="margin0 text-size-xs">{`${strings.bookOf} ${video.Book.name}, ${strings.site} ${video.site}`}</p>
                    {videoUrl &&
                    <VideoActionIcon className="flag" tooltip={'Is there an error in the video? Report it.'} glyphicon={'flag'} click={showReportModal} />
                    }
                  </Col>
                  <Col xs={1} className="text-center">
                    {(consecutive.nextVideo &&
                      <i onClick={() => this.changeVideo(consecutive.nextVideo.id)} className="fa fa-angle-right text-size-mlg clickable"></i>) ||
                      <i className="fa fa-angle-right text-size-mlg disabled color-gray-light"></i>
                    }
                  </Col>
                </Row>
              </Col>
              <Col xs={12} sm={3}>
                {video.RelatedVideos.map((relatedVideo) =>
                  <div onClick={() => this.changeVideo(relatedVideo.id)}>
                    <div className="mb8 text-center related-video-container">
                      <div className="related-video-image-container" onClick={this.handleOnVideoClick}>
                        <span className="related-video-image-overlay"/>
                        <img src={relatedVideo.imageUrl ? relatedVideo.imageUrl : '/images/video-placeholder.png'} alt="Placeholder" className={`video-placeholder related-video-image`} />
                      </div>
                      <div className="related-video-details">
                        <p className="margin0 text-uppercase text-size-xs">{relatedVideo.name}</p>
                        <p className="margin0 text-size-xxs">{`${strings.bookOf} ${relatedVideo.Book.name}, ${strings.site} ${relatedVideo.site}`}</p>
                      </div>
                    </div>
                  </div>)}
              </Col>
            </Row>
            <Row className="pb48">
              <Col xs={12} sm={7}>
                <h6>{strings.comments}</h6>
                <div className="comments-wrapper">
                {comments.map((comment) =>
                  <Row key={comment.id} className="pt16 comment">
                    <Col xs={3}>
                      <div className="text-center min-height">
                        <p className="margin0 text-bold ellipsised">{comment.User.firstName} {comment.User.lastName}</p>
                        <p className="ellipsised">{comment.User.city}</p>
                      </div>
                    </Col>
                    <Col xs={9}>
                      <div className="comment-text min-height pb16">
                        <p>{comment.comment}</p>
                        <p></p>
                      </div>
                    </Col>
                  </Row>)}
                </div>
                {profile.id && videoUrl &&
                <form className="pt24">
                  <FormGroup>
                    <FormControl componentClass="textarea" rows="4" placeholder={strings.comment.placeholder} {...comment} />
                  </FormGroup>
                  <Button disabled={_.keys(errors).length !== 0} type="button" onClick={this.handleCommentSubmit}>{strings.actions.comment}</Button>
                </form>}
              </Col>
            </Row>
          </div>
        }
      </div>
    );
  }
}
