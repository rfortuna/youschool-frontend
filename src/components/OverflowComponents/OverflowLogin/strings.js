/**
 * Created by rokfortuna on 17/08/16.
 */

export default {
  en: {
    login: 'Log In',
    closeModal: 'Close',
    rememberMe: 'Remember me',
    forgotPassword: 'Forgot password?',
    forgotUsername: 'Forgot username?',
    placeholders: {
      email: 'Email',
      password: 'Password',
    },
    errors: {
      required: 'Required.',
      invalidEmail: 'Email address is invalid.',
    },
    success: 'Success!',
    invalidCredentials: 'Invalid credentials!'
  },
  sv: {
    login: 'swe_Log In',
    closeModal: 'swe_Close',
    rememberMe: 'swe_Remember me',
    forgotPassword: 'swe_Forgot password?',
    forgotUsername: 'swe_Forgot username?',
    placeholders: {
      email: 'swe_Email',
      password: 'swe_Password',
    },
    errors: {
      required: 'swe_Required.',
      invalidEmail: 'swe_Email address is invalid.',
    },
    success: 'swe_Success!',
    invalidCredentials: 'swe_Invalid credentials!'
  }
};
