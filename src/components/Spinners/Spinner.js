/**
 * Created by rokfortuna on 12/02/16.
 */

import React, {Component, PropTypes} from 'react';

export default class Spinner extends Component {

  static propTypes = {
  }

  render() {
    return (
      <div className="spinner spinner-md">
        <i className="fa fa-spinner fa-spin">
        </i>
      </div>
    );
  }
}
