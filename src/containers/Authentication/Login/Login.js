/**
 * Created by rokfortuna on 3/24/16.
 */
import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import cookie from 'react-cookie';
import {Panel, Button} from 'react-bootstrap';

import {login} from 'redux/modules/authentication';
import {LoginForm} from 'components';
import {load as loadUser} from 'redux/modules/global/profile';
import config from 'config';
import {trackEvent as mixpanelTrackEvent} from 'redux/modules/global/mixpanel';
import strings from './strings.js';
import {toastr} from 'react-redux-toastr';


@connect(
  state => ({
    authenticationData: state.authentication,
    chosenLanguage: state.global.language.chosenLanguage
  }),
  dispatch => bindActionCreators({...routerActions, login , loadUser, mixpanelTrackEvent}, dispatch)
)
export default class Login extends Component {

  static propTypes = {
    authenticationData: PropTypes.object,
    login: PropTypes.func,
    loadUser: PropTypes.func,
    pushState: PropTypes.func,
    mixpanelTrackEvent: PropTypes.func,
    chosenLanguage: PropTypes.string
  };


  handleSubmit = (data) => {
    this.props.mixpanelTrackEvent('Login', {});
    const {login} = this.props;
    login(data.username, data.password).then((res) => {
      if (res.error) {
        toastr.error(res.error.error);
        console.log(`ERROR! ${res.error.error}`);
      } else {
        window.location.replace('/');
      }
    });
  };


  render() {
    const { chosenLanguage} = this.props;
    const stringsChosen = strings[chosenLanguage];

    return (
      <Panel>
        <LoginForm onSubmit={this.handleSubmit} chosenLanguage={chosenLanguage} />
        <Button href={`http://${config.apiHost}:${config.apiPort}/login/facebook`}>{`Facebook ${stringsChosen.login}`}</Button>
        <Button href={`http://${config.apiHost}:${config.apiPort}/login/google`}>{`Google ${stringsChosen.login}`}</Button>
      </Panel>
    );
  }
}
