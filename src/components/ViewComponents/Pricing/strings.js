export default {
  en: {
    pricing: 'Pricing',
    pricingSubtitle: 'Simple, flexible and affordable pricing plans',
    solutionVideos: 'Solution videos',
    mostPopular: 'Most popular',
    buyNow: 'Buy now'
  },
  sv: {
    pricing: 'swe_Pricing',
    pricingSubtitle: 'swe_Simple, flexible and affordable pricing plans',
    solutionVideos: 'swe_Solution videos',
    mostPopular: 'swe_Most popular',
    buyNow: 'swe_Buy now'
  }
}
