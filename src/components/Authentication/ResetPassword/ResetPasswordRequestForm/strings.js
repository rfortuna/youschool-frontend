export default {
  en: {
    errors: {
      required: 'Required.',
      invalidEmail: 'Email address is invalid.',
    },
    email: 'Email',
    enterEmailAddress: 'Enter Email address',
    resetPassword: 'Reset password'
  },
  sv: {
    errors: {
      required: 'swe_Required.',
      invalidEmail: 'swe_Email address is invalid.',
    },
    email: 'swe_Email',
    enterEmailAddress: 'swe_Enter Email address',
    resetPassword: 'swe_Reset password'
  }
}
