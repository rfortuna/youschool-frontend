/**
 * Created by rokfortuna on 12/02/16.
 */

import React, {Component, PropTypes} from 'react';

export default class StatisticComponent extends Component {

  static propTypes = {
    strings: PropTypes.object
  }

  render() {
    const {strings: {num, title, text}} = this.props;
    return (
      <div className="text-center">
        <h2 className="text-semi-bold">{num}</h2>
        <p className="text-size-sm">{title}</p>
        <div className="line" style={{margin: '16px auto'}}></div>
        <p>{text}</p>
      </div>
    );
  }
}
