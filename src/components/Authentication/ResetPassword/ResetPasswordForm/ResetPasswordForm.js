/**
 * Created by urbanmarovt on 15/04/16.
 */

import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {FormControl, FormGroup, Button, ControlLabel, HelpBlock} from 'react-bootstrap';
import {translateFormErrors} from 'utils/localization';
import _ from 'lodash';

import stringsOptions from './strings';

const validate = (values) => {

  const errors = {};

  if (!values.password) {
    errors.password = 'required';
  } else if (values.password.length < 8) {
    errors.password = 'passTooShort';
  }

  if (!values.confirmPassword) {
    errors.confirmPassword = 'required';
  }

  if (values.password !== values.confirmPassword) {
    errors.confirmPassword = 'passDontMatch';
  }

  return errors;

};

@reduxForm({
  form: 'ResetPasswordForm',
  fields: ['password', 'confirmPassword'],
  validate: validate
})
@connect(
  state => ({
    chosenLanguage: state.global.language.chosenLanguage,
    reseting: state.utils.resetPassword.resetingPassword
  })
)
export default class ResetPasswordForm extends Component {

  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    fields: PropTypes.object,
    errors: PropTypes.object
  }

  render() {
    const {fields: {password, confirmPassword}, errors, chosenLanguage, reseting} = this.props;
    const strings = stringsOptions[chosenLanguage];

    const translatedErrors = translateFormErrors(errors, strings.errors);

    return (
      <form onSubmit={this.props.handleSubmit}>
        <FormGroup validationState={ errors.password && password.touched ? 'error' : null}>
          <ControlLabel>{strings.password}</ControlLabel>
          <FormControl type="password" placeholder={strings.enterNewPassword} {...password} />
          {password.touched && errors.password && <HelpBlock>{translatedErrors.password}</HelpBlock>}
        </FormGroup>
        <FormGroup validationState={ errors.confirmPassword && confirmPassword.touched ? 'error' : null}>
          <ControlLabel>{strings.confirmPassword}</ControlLabel>
          <FormControl type="password" placeholder={strings.confirmNewPassword} {...confirmPassword} />
          {confirmPassword.touched && errors.confirmPassword && <HelpBlock>{translatedErrors.confirmPassword}</HelpBlock>}
        </FormGroup>
        <Button disabled={_.keys(errors).length !== 0} className="button-primary" type="submit">
          {(!reseting &&
          <span>{strings.resetPassword}</span>) ||
            <span><i className="fa fa-spinner fa-spin"></i></span>}
        </Button>
      </form>
    );
  }
}
