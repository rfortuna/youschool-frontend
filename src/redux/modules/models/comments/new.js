/**
 * Created by urbanmarovt on 10/08/16.
 */

const CREATE = 'youschool/comment/CREATE';
const CREATE_SUCCESS = 'youschool/comment/CREATE_SUCCESS';
const CREATE_FAIL = 'youschool/comment/CREATE_FAIL';

const initialState = {
  creating: false,
  error: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case CREATE:
      return {
        ...state,
        creating: false
      };
    case CREATE_SUCCESS:
      return {
        ...initialState,
      };
    case CREATE_FAIL:
      return {
        ...state,
        creating: false,
        error: action.error
      };
    default:
      return state;
  }
}

export function create(comment, videoId) {

  return {
    types: [CREATE, CREATE_SUCCESS, CREATE_FAIL],
    promise: (client, token) => client.post(`/videos/${videoId}/comments`, {
      token: token,
      data: {
        comment: comment
      }
    })
  };
}
