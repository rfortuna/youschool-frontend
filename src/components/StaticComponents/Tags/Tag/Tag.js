/**
 * Created by rokfortuna on 12/02/16.
 */

import React, {Component, PropTypes} from 'react';
import {Label} from 'react-bootstrap';

export default class Tag extends Component {

  static propTypes = {
    tag: PropTypes.object
  }

  render() {
    const {tag} = this.props;
    return (
      <span key={tag.id}>
        <Label style={{backgroundColor: tag.color}}>
          <i className="fa fa-tag"></i>
          {` ${tag.value}`}
        </Label>&nbsp;
      </span>
    );
  }
}
