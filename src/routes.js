import React from 'react';
import {IndexRoute, Route} from 'react-router';
import {
  App,
  RouterSecureContainer,

  NotFound,
  NotAuthorized,

  UserDetails,
  PricingContainer,
  Payment,

  Login,
  Register,
  ResetPassword,
  ResetPasswordRequest,
  VerifyEmailRedirect,
  AddUserData,

  Home,
  Landing,
  VideoDetails

} from 'containers';

export default (store) => {

  const requireLogin = (nextState, replaceState, callback) => {
    const { authentication: { token }} = store.getState();
    if (!token) {
      replaceState({nextPathname: nextState.location.pathname}, '/');
    }
    callback();
  }

  return (

    <Route path="/" component={App}>

      <IndexRoute component={Landing}/>
      <Route path="videos" component={Home}/>
      <Route path="videos/:videoId" component={VideoDetails} />

      {/* Pages that require login. */}
      <Route path="user" component={UserDetails} onEnter={requireLogin}/>
      <Route path="pricing" component={PricingContainer} onEnter={requireLogin}/>
      <Route path="payment/:planId" component={Payment} onEnter={requireLogin} />

      {/* Pages that do not require login. */}
      <Route path="resetPassword" component={ResetPasswordRequest} />
      <Route path="resetPassword/:token" component={ResetPassword} />
      <Route path="verifyEmail/:token" component={VerifyEmailRedirect} />

      <Route path="notAuthorized" component={NotAuthorized} status={401} />
      <Route path="*" component={NotFound} status={404} />

    </Route>

  );
};
