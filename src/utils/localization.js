export function translateFormErrors(errors, errorStrings, variables) {
  let translatedErrors = {};
  Object.keys(errors).forEach((key) => {
    if (Array.isArray(errorStrings[errors[key]])) {
      /*
        combining two arrays alternating
      */
      translatedErrors[key] = errorStrings[errors[key]].map((v, i) => [v, variables[errors[key]][i]].join(' ')).join(' ');
    } else {
      translatedErrors[key] = errorStrings[errors[key]];
    }
  });
  return translatedErrors;
}
