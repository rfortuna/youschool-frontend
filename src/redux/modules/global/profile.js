/**
 * Created by rokfortuna on 3/31/16.
 */

import cookie from 'react-cookie';
import {initIntercom} from 'utils/intercom';

const LOAD = 'youschool/global/profile/LOAD';
const LOAD_SUCCESS = 'youschool/global/profile/LOAD_SUCCESS';
const LOAD_FAIL = 'youschool/global/profile/LOAD_FAIL';
const LOAD_DATA = 'youschool/global/profile/LOAD_DATA';
const LOAD_DATA_SUCCESS = 'youschool/global/profile/LOAD_DATA_SUCCESS';
const LOAD_DATA_FAIL = 'youschool/global/profile/LOAD_DATA_FAIL';
const SET_PROFILE_VALUES = 'youschool/global/profile/SET_PROFILE_VALUES';
const UPDATE_CREDITS = 'youschool/global/profile/UPDATE_CREDITS';
const CLEAR_STATE = 'youschool/global/profile/CLEAR_STATE';

const SET_DEFAULT_BOOK = 'youschool/global/profile/SET_DEFAULT_BOOK';
const SET_DEFAULT_BOOK_SUCCESS = 'youschool/global/profile/SET_DEFAULT_BOOK_SUCCESS';
const SET_DEFAULT_BOOK_FAIL = 'youschool/global/profile/SET_DEFAULT_BOOK_FAIL';

const CLEAR_DEFAULT_BOOK = 'youschool/global/profile/CLEAR_DEFAULT_BOOK';
const CLEAR_DEFAULT_BOOK_SUCCESS = 'youschool/global/profile/CLEAR_DEFAULT_BOOK_SUCCESS';
const CLEAR_DEFAULT_BOOK_FAIL = 'youschool/global/profile/CLEAR_DEFAULT_BOOK_FAIL';

const SET_DEFAULT_BOOK_LOCAL = 'youschool/global/profile/SET_DEFAULT_BOOK_LOCAL';
const SET_DEFAULT_BOOK_LOCAL_ID = 'youschool/global/profile/SET_DEFAULT_BOOK_LOCAL_ID';

const initialState = {
  id: null,
  loading: false,
  DefaultBookId: null,
  DefaultBook: null,
  Country: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      initIntercom(action.result.body.user);
      return {
        ...state,
        loading: false,
        ...action.result.body.user,
      };
    case LOAD_FAIL:
      console.log('LOAD PROFILE FAIL');
      cookie.remove('auth_token', { path: '/' });
      cookie.remove('refresh_token', { path: '/' });
      cookie.remove('token_expires_at', { path: '/' });
      return initialState;
    case UPDATE_CREDITS:
      return {
        ...state,
        credits: action.data.credits
      };
    case SET_DEFAULT_BOOK:
      return state;
    case SET_DEFAULT_BOOK_SUCCESS:
      return {
        ...state,
        ...action.result.body[1][0]
      };
    case SET_DEFAULT_BOOK_FAIL:
      return state;
    case CLEAR_STATE:
      return initialState;
    case SET_DEFAULT_BOOK_LOCAL:
      cookie.save('DefaultBookId', action.data.DefaultBook.id, { path: '/' });
      return {
        ...state,
        DefaultBookId: action.data.DefaultBook ? action.data.DefaultBook.id : null,
        DefaultBook: action.data.DefaultBook
      };
    case CLEAR_DEFAULT_BOOK:
    case CLEAR_DEFAULT_BOOK_SUCCESS:
    case CLEAR_DEFAULT_BOOK_FAIL:
      cookie.remove('DefaultBookId', { path: '/' });
      return {
        ...state,
        DefaultBookId: null,
        DefaultBook: null
      };
    case SET_DEFAULT_BOOK_LOCAL_ID:
      return {
        ...state,
        DefaultBookId: action.data.DefaultBookId
      };
    default:
      return state;
  }
}

export function load() {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token) => client.get(`/profile`, {token: token})
  };
}

export function isLoaded(globalState) {
  return globalState.global.profile.id !== null;
}

export function clear() {
  return {
    type: CLEAR_STATE
  }
}

export function setDefaultBookProfile(bookId) {
  const formData = {DefaultBookId: bookId};
  return {
    types: [SET_DEFAULT_BOOK, SET_DEFAULT_BOOK_SUCCESS, SET_DEFAULT_BOOK_FAIL],
    promise: (client, token) => client.put(`/profile`, {token: token, formData: formData})
  };
}

export function clearDefaultBookProfile() {
  return {
    types: [CLEAR_DEFAULT_BOOK, CLEAR_DEFAULT_BOOK_SUCCESS, CLEAR_DEFAULT_BOOK_FAIL],
    promise: (client, token) => client.post(`/profile/clearDefaultBook`, {token: token})
  };
}

export function setDefaultBookLocal(Book) {
  return {
    type: SET_DEFAULT_BOOK_LOCAL,
    data: {DefaultBook: Book}
  };
}

export function setDefaultBookLocalId(bookId) {
  return {
    type: SET_DEFAULT_BOOK_LOCAL_ID,
    data: {DefaultBookId: bookId}
  };
}

export function updateCredits(credits) {
  return {
    type: UPDATE_CREDITS,
    data: {credits: credits}
  }
}
