/**
 * Created by rokfortuna on 4/27/16.
 */

export default {
  en: {
    modalTitle: 'Report error',
    closeModal: 'Close',
    submit: 'Submit',
    video: 'Video',
    book: 'Book',
    onlyReportIfSure: 'Please, only report the error if you are sure.',
    descriptionOfError: 'Describe your issue...',
    description: 'Description'
  },
  sv: {
    modalTitle: 'swe_Report error',
    closeModal: 'swe_Close',
    submit: 'swe_Submit',
    video: 'swe_Video',
    book: 'swe_Book',
    onlyReportIfSure: 'swe_Please, only report the error if you are sure.',
    descriptionOfError: 'swe_Describe your issue...',
    description: 'swe_Description'
  }
};
