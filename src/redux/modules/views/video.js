/**
 * Created by urbanmarovt on 24/04/16.
 */

/**
 * Created by urbanmarovt on 15/04/16.
 */

const SHOW_PURCHASE_MODAL = 'youschool/views/video/SHOW_PURCHASE_MODAL';
const CLOSE_PURCHASE_MODAL = 'youschool/views/video/CLOSE_PURCHASE_MODAL';

const SHOW_BUY_CREDITS_MODAL = 'youschool/views/video/SHOW_BUY_CREDITS_MODAL';
const CLOSE_BUY_CREDITS_MODAL = 'youschool/views/video/CLOSE_BUY_CREDITS_MODAL';

const SHOW_RATE_MODAL = 'youschool/views/video/SHOW_RATE_MODAL';
const CLOSE_RATE_MODAL = 'youschool/views/video/CLOSE_RATE_MODAL';

const SHOW_REPORT_MODAL = 'youschool/views/video/SHOW_REPORT_MODAL';
const CLOSE_REPORT_MODAL = 'youschool/views/video/CLOSE_REPORT_MODAL';

const initialState = {
  purchaseModalVisible: false,
  buyCreditsModalVisible: false,
  rateModalVisible: false,
  reportModalVisible: false
};

export default function reducer(state = initialState, action = {}) {

  switch (action.type) {
    case SHOW_PURCHASE_MODAL:
      return {
        ...state,
        purchaseModalVisible: true
      };
    case CLOSE_PURCHASE_MODAL:

      return {
        ...state,
        purchaseModalVisible: false
      };
    case SHOW_BUY_CREDITS_MODAL:
      return {
        ...state,
        buyCreditsModalVisible: true
      };
    case CLOSE_BUY_CREDITS_MODAL:
      return {
        ...state,
        buyCreditsModalVisible: false
      };
    case SHOW_RATE_MODAL:
      return {
        ...state,
        rateModalVisible: true
      };
    case CLOSE_RATE_MODAL:
      return {
        ...state,
        rateModalVisible: false
      };
    case SHOW_REPORT_MODAL:
      return {
        ...state,
        reportModalVisible: true
      };
    case CLOSE_REPORT_MODAL:
      return {
        ...state,
        reportModalVisible: false
      };
    default:
      return state;
  }
}

export function showPurchaseModal() {
  return {
    type: SHOW_PURCHASE_MODAL
  };
}

export function hidePurchaseModal() {
  return {
    type: CLOSE_PURCHASE_MODAL
  };
}

export function showBuyCreditsModal() {
  return {
    type: SHOW_BUY_CREDITS_MODAL
  };
}

export function hideBuyCreditsModal() {
  return {
    type: CLOSE_BUY_CREDITS_MODAL
  };
}

export function showRateModal() {
  return {
    type: SHOW_RATE_MODAL
  };
}

export function hideRateModal() {
  return {
    type: CLOSE_RATE_MODAL
  };
}

export function showReportModal() {
  return {
    type: SHOW_REPORT_MODAL
  };
}

export function hideReportModal() {
  return {
    type: CLOSE_REPORT_MODAL
  };
}

