/**
 * Created by urbanmarovt on 25/04/16.
 */

import { combineReducers } from 'redux';

import plansList from './list';
import planDetails from './details';

export default combineReducers({
  list: plansList,
  details: planDetails
});