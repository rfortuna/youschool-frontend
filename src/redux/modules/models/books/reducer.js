/**
 * Created by rokfortuna on 4/23/16.
 */

import { combineReducers } from 'redux';

import booksList from './list';

export default combineReducers({
  list: booksList
});
