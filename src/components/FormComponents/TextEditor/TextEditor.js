/**
 * Created by urbanmarovt on 23/02/16.
 */

import React, {Component, PropTypes} from 'react';
import ReactQuill from 'react-quill';

import styles from './TextEditor.scss';

export default class TextEditor extends Component {

  static propTypes = {
    data: PropTypes.array,
    labelClassName: PropTypes.string,
    wrapperClassName: PropTypes.string,
    label: PropTypes.string,
    onBlur: PropTypes.func,
    touched: PropTypes.bool,
    error: PropTypes.string,
    value: PropTypes.string,
    placeholder: PropTypes.string
  };

  render() {
    const {labelClassName, wrapperClassName, value, onBlur, label, error, touched, ...props} = this.props;

    return (
      <div className={`${styles.marginBottom} row`}>
        {label &&
        <div className={labelClassName}>
          <label className={`${styles.label} ${touched && error && styles.error}`}>{ label }</label>
        </div>}
        <div className={wrapperClassName}>
          { __CLIENT__ &&
          <ReactQuill
            theme="snow"
            value={value}
            onBlur = {() => onBlur(value)}
            {...props}
          /> }
          {touched && error &&
          <div className={styles.error}>
            {error}
          </div>}
        </div>
      </div>
    );
  }
}
