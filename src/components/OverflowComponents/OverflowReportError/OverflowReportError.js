/**
 * Created by rokfortuna on 4/27/16.
 */

import React, {Component, PropTypes} from 'react';
import {Button, Modal, Row, Col, Glyphicon, FormControl, FormGroup, ControlLabel } from 'react-bootstrap';
import {reduxForm} from 'redux-form';
import {connect} from 'react-redux';

import multiStrings from './strings';

@connect(
  state => ({
    chosenLanguage: state.global.language.chosenLanguage,
  })
)
@reduxForm({
  form: 'ReportErrorForm',
  fields: ['description']
})
export default class OverflowReportError extends Component {

  static propTypes = {
    chosenLanguage: PropTypes.string,
    handleCancel: PropTypes.func.isRequired,
    reportError: PropTypes.func,
    visible: PropTypes.bool,
    video: PropTypes.object
  };

  render() {
    const {visible, handleCancel, chosenLanguage, video, fields: {description}, reportError} = this.props;
    const strings = multiStrings[chosenLanguage];

    return (
    <form>
      <Modal show={visible} onHide={handleCancel}>
        <Modal.Header closeButton>
          <Modal.Title>{strings.modalTitle}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>{strings.video}: <strong>{video.name}</strong></p>
          <p>{strings.book}: <strong>{video.Book.name}</strong></p>
          <p><strong>{strings.onlyReportIfSure}</strong></p>
          <FormGroup>
            <ControlLabel>{strings.description}</ControlLabel>
            <FormControl componentClass="textarea" placeholder={strings.descriptionOfError} {...description} />
          </FormGroup>
        </Modal.Body>
        <Modal.Footer>
          <Row>
            <Col xs={12} sm={6}>
              <Button className="pull-left full-width button-primary" onClick={reportError(description)}>
                {strings.submit}
              </Button>
            </Col>
          </Row>
        </Modal.Footer>
      </Modal>
    </form>
    );
  }
}
