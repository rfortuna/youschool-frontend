/**
 * Created by urbanmarovt on 15/04/16.
 */

const RESET_PASS = 'youschool/authentication/RESET_PASS';
const RESET_PASS_SUCCESS = 'youschool/authentication/RESET_PASS_SUCCESS';
const RESET_PASS_FAIL = 'youschool/authentication/RESET_PASS_FAIL';

const RESET_PASS_REQUEST = 'youschool/verifyEmail/RESET_PASS_REQUEST';
const RESET_PASS_REQUEST_SUCCESS = 'youschool/verifyEmail/RESET_PASS_REQUEST_SUCCESS';
const RESET_PASS_REQUEST_FAIL = 'youschool/verifyEmail/RESET_PASS_REQUEST_FAIL';


const initialState = {
  error: {},
  requestingResetPassword: false,
  resetPasswordRequested: false,
  resetingPassword: false,
  passwordReseted: false
};

export default function reducer(state = initialState, action = {}) {

  switch (action.type) {
    case RESET_PASS:
      return {
        ...state,
        resetingPassword: true
      };
    case RESET_PASS_SUCCESS:

      return {
        ...state,
        resetingPassword: false,
        passwordReseted: true
      };
    case RESET_PASS_FAIL:
      return {
        ...state,
        resetingPassword: false,
        error: action.error
      };
    case RESET_PASS_REQUEST:
      return {
        ...state,
        requestingResetPassword: true
      };
    case RESET_PASS_REQUEST_SUCCESS:
      return {
        ...state,
        resetPasswordRequested: true,
        requestingResetPassword: false
      };
    case RESET_PASS_REQUEST_FAIL:
      return {
        ...state,
        requestingResetPassword: false,
        error: action.error
      };
    default:
      return state;
  }
}

export function requestResetPassword(data) {
  return {
    types: [RESET_PASS_REQUEST, RESET_PASS_REQUEST_SUCCESS, RESET_PASS_REQUEST_FAIL],
    promise: (client) => client.post(`/lostPass/request`, {
      data: {
        email: data.email
      }
    })
  };
}

export function resetPassword(data, token) {
  return {
    types: [RESET_PASS, RESET_PASS_SUCCESS, RESET_PASS_FAIL],
    promise: (client) => client.post('/lostPass/reset', {
      data: {
        password: data.password,
        token: token
      }
    })
  }
}
