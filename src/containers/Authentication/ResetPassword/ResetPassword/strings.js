export default {
  en: {
    passTokenInvalid: 'Your reset password token is not valid!' ,
    passSuccess: 'Your password has been successfully reset.',
    resetPassword: 'Reset password'
  },
  sv: {
    passTokenInvalid: 'swe_Your reset password token is not valid!' ,
    passSuccess: 'swe_Your password has been successfully reset.',
    resetPassword: 'swe_Reset password'
  }
}
