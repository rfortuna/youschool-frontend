/**
 * Created by rokfortuna on 4/14/16.
 */

import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import {Panel} from 'react-bootstrap';
import {toastr} from 'react-redux-toastr';

import {ResetPasswordForm} from 'components';
import {resetPassword} from 'redux/modules/utils/resetPassword';
import stringsOptions from './strings';


@connect(
  state => ({
    chosenLanguage: state.global.language.chosenLanguage,
  }),
  dispatch => bindActionCreators({...routerActions, resetPassword}, dispatch)
)
export default class Login extends Component {

  static propTypes = {
    pushState: PropTypes.func
  };

  handleSubmit = (data) => {
    const {resetPassword, params: {token}, chosenLanguage, pushState} = this.props;
    const strings = stringsOptions[chosenLanguage];

    resetPassword(data, token).then((res) => {
      if (res.error) {
        toastr.error(strings.passTokenInvalid);
      } else {
        toastr.success(strings.passSuccess);
      }
      pushState(null, '/');
    });
  };

  render() {
    const {chosenLanguage} = this.props;
    const strings = stringsOptions[chosenLanguage];

    return (
      <Panel className="reset-password-container">
        <h3 className="mb24"> {strings.resetPassword} </h3>
        <ResetPasswordForm onSubmit={this.handleSubmit} />
      </Panel>
    );
  }
}
