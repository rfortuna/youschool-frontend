/**
 * Created by rokfortuna on 3/31/16.
 */

import { combineReducers } from 'redux';

import profile from './profile';
import mixpanel from './mixpanel';
import language from './language';

export default combineReducers({
  profile: profile,
  mixpanel: mixpanel,
  language: language
});
