/**
 * Created by rokfortuna on 4/16/16.
 */

import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import {Panel, Button} from 'react-bootstrap';

import {trackEvent as mixpanelTrackEvent, setProfile as mixpanelSetProfile} from 'redux/modules/global/mixpanel';
import {UserForm} from 'components';
import {update as updateUser} from 'redux/modules/models/users/details';

@connect(
  state => (
  {
    user: state.global.user
  }),
  dispatch => bindActionCreators({...routerActions, updateUser, mixpanelTrackEvent, mixpanelSetProfile}, dispatch)
)
export default class AddUserData extends Component {

  static propTypes = {
    updateUser: PropTypes.func,
    pushState: PropTypes.func,
    mixpanelTrackEvent: PropTypes.func,
    user: PropTypes.object,

  };


  handleSubmit = (data) => {
    const {updateUser, mixpanelSetProfile} = this.props;
    updateUser(data).then((res) => {
      if (res.error) {
        console.log(`ERROR! ${res.error.error}`);
      } else {
        console.log(`SUCCESS`);
        mixpanelSetProfile(data.id, data);
      }
    });
  };


  render() {
    const {user} = this.props;
    return (
      <Panel>
        <UserForm onSubmit={this.handleSubmit} user={user} />
      </Panel>
    );
  }
}
