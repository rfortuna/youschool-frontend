/**
 * Created by urbanmarovt on 25/04/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import {load as loadPlans} from 'redux/modules/models/plans/list';
import {Panel, Row, Col} from 'react-bootstrap';

import multiStrings from './strings';

function fetchDataDeferred(getState, dispatch) {
  return dispatch(loadPlans());
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    profile: state.global.profile,
    allPlans: state.models.plans.list.data
  }),
  dispatch => bindActionCreators({...routerActions}, dispatch)
)
export default class BuyCredits extends Component {

  static propTypes = {
    profile: PropTypes.object
  };

  render() {
    const {allPlans, profile} = this.props;

    const strings = multiStrings.eng;

    return (
      <Row>
        <Col lg={12}>
          <h2>{`Profile credits: ${profile.credits}`}</h2>
        </Col>
        { allPlans.map((plan) =>
          <Col md={4} sm={6} xs={12} key={plan.id} onClick={() => this.props.pushState(null, `/buyCredits/${plan.id}`)} className="clickable">
            <Panel>
              <h4>{plan.name}</h4>
              <img src={plan.logo} />
              <h6>{`Price: ${plan.amount} ${plan.currency}`}</h6>
              <h5>{`Credits: ${plan.credits}`}</h5>
            </Panel>
          </Col>)}
      </Row>
    );
  }
}
