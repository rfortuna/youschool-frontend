/**
 * Created by urbanmarovt on 14/04/16.
 */

import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import cookie from 'react-cookie';
import {Panel} from 'react-bootstrap';

import {createAccount, verifyEmail} from 'redux/modules/authentication';
import {verifyEmailRequest} from 'redux/modules/utils/verifyEmail';
import {RegisterForm} from 'components';

import {toastr} from 'react-redux-toastr';

@connect(
  state => ({
    chosenLanguage: state.global.language.chosenLanguage
  }),
  dispatch => bindActionCreators({...routerActions, createAccount, verifyEmailRequest}, dispatch)
)
export default class Register extends Component {

  static propTypes = {
    authenticationData: PropTypes.object,
    createAccount: PropTypes.func,
    pushState: PropTypes.func,
    verifyEmailRequest: PropTypes.func,
    chosenLanguage: PropTypes.string,
    pushState: PropTypes.func
  };

  handleSubmit = (data) => {
    const {createAccount, verifyEmailRequest, pushState} = this.props;
    createAccount(data).then((res) => {
      return verifyEmailRequest(res.result.body.id);
    }).then((res) => {
      toastr.success('You have successfully created an account. Check your email for to verify your account!');
      pushState(null, '/');
    });
  };

  render() {
    const {chosenLanguage} = this.props;
    return (
      <Panel>
        <RegisterForm onSubmit={this.handleSubmit} chosenLanguage={chosenLanguage}/>
      </Panel>
    );
  }
}
