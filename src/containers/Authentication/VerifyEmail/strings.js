export default {
  en: {
    success: 'You have successfully verified your email address.',
    youWillBeRedirected: (countSeconds) => `You will be redirected to home in ${countSeconds} seconds.`,
    tokenExpired: 'Your email verification token has expired. Log in and resend verification email.'
  },
  sv: {
    success: 'swe_You have successfully verified your email address.',
    youWillBeRedirected: (countSeconds) => `swe_You will be redirected to home in ${countSeconds} seconds.`,
    tokenExpired: 'swe_Your email verification token has expired. Log in and resend verification email.'
  }
}
