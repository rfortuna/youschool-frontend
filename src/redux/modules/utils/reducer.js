/**
 * Created by urbanmarovt on 15/04/16.
 */

import verifyEmail from './verifyEmail';
import resetPassword from './resetPassword';
import payment from './payment';
import { combineReducers } from 'redux';
export default combineReducers({
  verifyEmail: verifyEmail,
  resetPassword: resetPassword,
  payment: payment
});
