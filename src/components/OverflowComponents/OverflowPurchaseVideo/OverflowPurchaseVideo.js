/**
 * Created by urbanmarovt on 24/04/16.
 */


import React, {Component, PropTypes} from 'react';
import {Button, Modal, Row, Col} from 'react-bootstrap';
import {connect} from 'react-redux';

import multiStrings from './strings';

@connect(
  state => ({
    chosenLanguage: state.global.language.chosenLanguage,
  })
)
export default class OverflowPurchaseVideo extends Component {

  static propTypes = {
    chosenLanguage: PropTypes.string,
    handleCancel: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    visible: PropTypes.bool
  };

  render() {
    const {visible, handleSubmit, handleCancel, chosenLanguage, video} = this.props;
    const strings = multiStrings[chosenLanguage];

    return (
      <Modal show={visible} onHide={handleCancel}>
        <Modal.Header closeButton>
          <Modal.Title>{strings.purchaseVideo}: {video.name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>{strings.exerciseDifficulty}: <strong>{video.Difficulty.type}</strong></p>
          <p>{strings.book}: <strong>{video.Book.name}</strong></p>
          <p>{strings.numOfCredits}: <strong>{video.credits}</strong></p>
        </Modal.Body>
        <Modal.Footer>
          <Row>
            <Col xs={12} sm={6}>
              <Button className="pull-left full-width button-primary" onClick={handleSubmit}>
                {strings.actions.purchaseVideo}
              </Button>
            </Col>
          </Row>
        </Modal.Footer>
      </Modal>
    );
  }
}
