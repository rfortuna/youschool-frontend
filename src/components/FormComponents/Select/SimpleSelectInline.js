/**
 * Created by urbanmarovt on 24/04/16.
 */

import React, {Component, PropTypes} from 'react';
import Select from 'react-select';
import {FormGroup, ControlLabel} from 'react-bootstrap';
import styles from './Selects.scss';

export default class SimpleSelectInline extends Component {

  static propTypes = {
    data: PropTypes.array,
    labelClassName: PropTypes.string,
    wrapperClassName: PropTypes.string,
    label: PropTypes.string,
    onBlur: PropTypes.func,
    touched: PropTypes.bool,
    error: PropTypes.string,
    value: PropTypes.string,
    placeholder: PropTypes.string
  };

  render() {
    const {labelClassName, wrapperClassName, placeholder, onChange, data, value, onBlur, label, error, touched, handleOnChange, ...props} = this.props;

    let selectOnChange;
    if(handleOnChange) {
      selectOnChange = (value) => {
        this.setState({value});
        onChange(value);
        handleOnChange(value);
      }
    } else {
      selectOnChange = (value) => {
        this.setState({value});
        onChange(value);
      }
    }

    return (
      <div>
        {label &&
        <ControlLabel>{label}</ControlLabel>
        }
        <Select
          {...props}
          className={styles.select}
          onChange={selectOnChange}
          value={value}
          onBlur={() => onBlur(value)}
          placeholder={placeholder || ''}
          options = {data}/>
      </div>
    );
  }
}
