/**
 * Created by rokfortuna on 4/26/16.
 */


import React, {Component, PropTypes} from 'react';
import {OverlayTrigger, Tooltip, Glyphicon} from 'react-bootstrap';
import styles from './VideoActionIcon.scss';

export default class VideoActionIcon extends Component {

  static propTypes = {
    tooltip: PropTypes.string,
    glyphicon: PropTypes.string,
    click: PropTypes.func
  }

  render() {
    const {tooltip, glyphicon, click} = this.props;
    return (
      <OverlayTrigger placement="bottom"
                      overlay={
                      <Tooltip className="in">
                        {tooltip}
                      </Tooltip>}>
              <span className={styles.videoGlyphicon}>
                <Glyphicon glyph={glyphicon} className="clickable" onClick={click}/>
              </span>
      </OverlayTrigger>
    );
  }
}
