/**
 * Created by urbanmarovt on 25/04/16.
 */
/**
 * Created by urbanmarovt on 24/04/16.
 */


import React, {Component, PropTypes} from 'react';
import {Button, Modal, Row, Col} from 'react-bootstrap';
import {connect} from 'react-redux';

import multiStrings from './strings';

@connect(
  state => ({
    chosenLanguage: state.global.language.chosenLanguage,
  })
)
export default class OverflowBuyCredits extends Component {

  static propTypes = {
    chosenLanguage: PropTypes.string,
    handleCancel: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    visible: PropTypes.bool
  };

  render() {
    const {visible, handleSubmit, handleCancel, chosenLanguage} = this.props;
    const strings = multiStrings[chosenLanguage];

    return (
      <Modal show={visible} onHide={handleCancel}>
        <Modal.Header closeButton>
          <Modal.Title>{strings.buyCredits}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>{strings.hint}</p>
        </Modal.Body>
        <Modal.Footer>
          <Row>
            <Col xs={12} sm={6}>
              <Button className="pull-left full-width button-primary" onClick={handleSubmit}>
                {strings.actions.buyCredits}
              </Button>
            </Col>
          </Row>
        </Modal.Footer>
      </Modal>
    );
  }
}
