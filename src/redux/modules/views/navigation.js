/**
 * Created by urbanmarovt on 17/08/16.
 */


const SHOW_REGISTER_MODAL = 'youschool/views/navigation/SHOW_REGISTER_MODAL';
const CLOSE_REGISTER_MODAL = 'youschool/views/navigation/CLOSE_REGISTER_MODAL';

const SHOW_LOGIN_MODAL = 'youschool/views/navigation/SHOW_LOGIN_MODAL';
const CLOSE_LOGIN_MODAL = 'youschool/views/navigation/CLOSE_LOGIN_MODAL';

const initialState = {
  registerModalVisible: false,
  loginModalVisible: false
};

export default function reducer(state = initialState, action = {}) {

  switch (action.type) {
    case SHOW_REGISTER_MODAL:
      return {
        ...state,
        registerModalVisible: true
      };
    case CLOSE_REGISTER_MODAL:
      return {
        ...state,
        registerModalVisible: false
      };
      case SHOW_LOGIN_MODAL:
        return {
          ...state,
          loginModalVisible: true
        };
      case CLOSE_LOGIN_MODAL:
        return {
          ...state,
          loginModalVisible: false
        };
    default:
      return state;
  }
}

export function showRegisterModal() {
  return {
    type: SHOW_REGISTER_MODAL
  };
}

export function hideRegisterModal() {
  return {
    type: CLOSE_REGISTER_MODAL
  };
}

export function showLoginModal() {
  return {
    type: SHOW_LOGIN_MODAL
  };
}

export function hideLoginModal() {
  return {
    type: CLOSE_LOGIN_MODAL
  };
}
