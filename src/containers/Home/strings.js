/**
 * Created by rokfortuna on 4/26/16.
 */

export default {
  sv : {
    videoNamePlaceholder: 'swe_Task name...',
    page: 'swe_Page'
  },
  en: {
    videoNamePlaceholder: 'Task name...',
    page: 'Page'
  }
};
