export default {
  en: {
    resetPassword: 'Reset password',
    checkYourEmail: 'Check your email in order to reset your password.',
    email: 'Email',
    emailNotFound: 'Email address was not found.'
  },
  sv: {
    resetPassword: 'swe_Reset password',
    checkYourEmail: 'swe_Check your email in order to reset your password.',
    email: 'swe_Email',
    emailNotFound: 'swe_Email address was not found.'
  }
}
