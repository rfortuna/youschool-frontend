/**
 * Created by urbanmarovt on 24/04/16.
 */

export default {
  en: {
    site: 'Page',
    exercise: 'exercise',
    bookOf: 'Book of',
    comments: 'Comments',
    comment: {
      placeholder: 'Write your comment on this exercise ...'
    },
    actions: {
      comment: 'Comment'
    },
    errors: {
      required: 'Required'
    },
    toastrs: {
      reportErrorSuccess: 'Thank you very much for making this portal better. Your report has been successfully submitted.',
      reportPurchaseSuccess: 'Video purchased!',
      reportPurchaseFailure: 'There was an error when purchasing the video.'

    }
  },
  sv: {
    site: 'swe_Page',
    exercise: 'swe_exercise',
    bookOf: 'swe_Book of',
    comments: 'swe_Comments',
    comment: {
      placeholder: 'swe_Write your comment on this exercise ...'
    },
    actions: {
      comment: 'swe_Comment'
    },
    errors: {
      required: 'swe_Required'
    },
    toastrs: {
      reportErrorSuccess: 'swe_Thank you very much for making this portal better. Your report has been successfully submitted.',
      reportPurchaseSuccess: 'swe_Video purchased!',
      reportPurchaseFailure: 'swe_There was an error when purchasing the video.'
    }
  }
};
