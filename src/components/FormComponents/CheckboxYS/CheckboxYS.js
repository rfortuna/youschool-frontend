/**
 * Created by urbanmarovt on 10/02/16.
 */

import React, {Component, PropTypes} from 'react';


export default class CheckboxYS extends Component {

  static propTypes = {
    handleChange: PropTypes.func
  }

  handleOnClick = () => {
    const {handleChange, value, onChange} = this.props;
    if (handleChange) {
        handleChange(!value);
    }
    onChange(!value);
  }

  render() {
    const {value} = this.props;

    return (
      <span onClick={this.handleOnClick} className="checkbox-ys">
        { !value &&
            <img className="unchecked" src="/images/checkbox_unchecked.png"/>
        }
        { value &&
          <img className="checked" src="/images/checkbox_checked.png"/>
        }
      </span>
    );
  }
}
