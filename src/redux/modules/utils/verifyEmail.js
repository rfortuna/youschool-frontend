/**
 * Created by urbanmarovt on 15/04/16.
 */
/**
 * Created by rokfortuna on 3/24/16.
 */
import cookie from 'react-cookie';

const VERIFY_EMAIL = 'youschool/verifyEmail/VERIFY_EMAIL';
const VERIFY_EMAIL_SUCCESS = 'youschool/verifyEmail/VERIFY_EMAIL_SUCCESS';
const VERIFY_EMAIL_FAIL = 'youschool/verifyEmail/VERIFY_EMAIL_FAIL';

const CONFIRM_VERIFY_EMAIL = 'youschool/verifyEmail/CONFIRM_VERIFY_EMAIL';
const CONFIRM_VERIFY_EMAIL_SUCCESS = 'youschool/verifyEmail/CONFIRM_VERIFY_EMAIL_SUCCESS';
const CONFIRM_VERIFY_EMAIL_FAIL = 'youschool/verifyEmail/CONFIRM_VERIFY_EMAIL_FAIL';


const initialState = {
  error: {},
  requestingVerifyEmail: false,
  verifyEmailRequested: false,
  confirmingVerifyEmail: false,
  emailConfirmed: false
};

export default function reducer(state = initialState, action = {}) {

  switch (action.type) {
    case VERIFY_EMAIL:
      return {
        ...state,
        requestingVerifyEmail: true
      };
    case VERIFY_EMAIL_SUCCESS:

      return {
        ...state,
        requestingVerifyEmail: false,
        verifyEmailRequested: true
      };
    case VERIFY_EMAIL_FAIL:
      return {
        ...state,
        requestingVerifyEmail: false,
        error: action.error
      };
    case CONFIRM_VERIFY_EMAIL:
      return {
        ...state,
        confirmingVerifyEmail: true
      };
    case CONFIRM_VERIFY_EMAIL_SUCCESS:
      return {
        ...state,
        emailConfirmed: true,
        confirmingVerifyEmail: false
      };
    case CONFIRM_VERIFY_EMAIL_FAIL:
      return {
        ...state,
        confirmingVerifyEmail: false,
        error: action.error
      };
    default:
      return state;
  }
}

export function verifyEmailRequest() {
  return {
    types: [VERIFY_EMAIL, VERIFY_EMAIL_SUCCESS, VERIFY_EMAIL_FAIL],
    promise: (client, token) => client.post(`/verifyEmail`, {
     token: token
    })
  };
}

export function verifyEmailComfirm(token) {
  return {
    types: [CONFIRM_VERIFY_EMAIL, CONFIRM_VERIFY_EMAIL_SUCCESS, CONFIRM_VERIFY_EMAIL_FAIL],
    promise: (client) => client.get('/verifyEmail/confirm', {
      params: {
        token: token
      }
    })
  }
}

