export default {
  en: {
    searchTaskPlaceholder: 'Search tasks...',
    profile: 'Profile',
    logout: 'Logout',
    login: 'Login',
    register: 'Register'
  },
  sv: {
    searchTaskPlaceholder: 'Search tasks...',
    profile: 'swe_Profile',
    logout: 'swe_Logout',
    login: 'swe_Login',
    register: 'swe_Register'
  }
}
