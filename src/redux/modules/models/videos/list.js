/**
 * Created by rokfortuna on 4/23/16.
 */

const LOAD = 'youschool/videos/LOAD';
const LOAD_SUCCESS = 'youschool/videos/LOAD_SUCCESS';
const LOAD_FAIL = 'youschool/videos/LOAD_FAIL';

const LOAD_SELECT_OPTIONS = 'youschool/videos/LOAD_SELECT_OPTIONS';
const LOAD_SELECT_OPTIONS_SUCCESS = 'youschool/videos/LOAD_SELECT_OPTIONS_SUCCESS';
const LOAD_SELECT_OPTIONS_FAIL = 'youschool/videos/LOAD_SELECT_OPTIONS_FAIL';

const CLEAR_SELECT_OPTIONS = 'youschool/videos/CLEAR_SELECT_OPTIONS';

const initialState = {
  loaded: false,
  editing: {},
  error: {},
  data: [],
  count: 0,
  // for the video select on main page and top search on videos
  selectOptions: {
    data: [],
    loading: false
  }
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        data: [],
        loading: true
      };
    case LOAD_SUCCESS:
      const newCount = action.result.headers['content-range'];
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.body,
        // typeof newCount == 'undefined' ? state.count : parseInt(newCount)
        count: parseInt(newCount),
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: [],
        count: 0,
        error: action.error
      };
      case LOAD_SELECT_OPTIONS:
        return {
          ...state,
          selectOptions: {
            ...state.selectOptions,
            loading: true
          }
        };
      case LOAD_SELECT_OPTIONS_SUCCESS:
        return {
          ...state,
          selectOptions: {
            data: action.result.body,
            loading: false
          },
        };
      case LOAD_SELECT_OPTIONS_FAIL:
        return {
          ...state,
          selectOptions: {
            ...state.selectOptions,
            loading: false
          },
        };
      case CLEAR_SELECT_OPTIONS:
        return {
          ...state,
          selectOptions: {
            data: [],
            loading: false
          }
        };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.models.videos.list && globalState.models.videos.list.loaded;
}

// put globalState as parameters
export function load(queryParams, bookId, name) {
  const {count, offset} = queryParams;

  const params = {
    ...queryParams,
    count: count,
    offset: offset,
    name: name,
  };

  if (bookId) {
    params.bookId = bookId;
  }

  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token) => client.get(`/videos`, { params: params, token: token})
  };
}

// put globalState as parameters
export function loadOptions(name, bookId) {

  const params = {
    name: name,
    bookId: bookId,
    // SHOW 10 videos by default, user needs to be more specific
    count: 10
  };

  return {
    types: [LOAD_SELECT_OPTIONS, LOAD_SELECT_OPTIONS_SUCCESS, LOAD_SELECT_OPTIONS_FAIL],
    promise: (client, token) => client.get(`/videos`, { params: params, token: token})
  };
}

export function clearSelectOptions() {
  return {
    type: CLEAR_SELECT_OPTIONS
  }
}
