/**
 * Created by rokfortuna on 23/05/16.
 */

import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {reduxForm} from 'redux-form';
import {CustomSelect} from 'components';
import {setLanguage} from 'redux/modules/global/language';


@connect(
  state => ({
    languageOptions: state.global.language.supportedLanguages
  }),
  dispatch => bindActionCreators({setLanguage}, dispatch)
)
@reduxForm({
    form: 'LanguageForm',
    fields: ['language']
  },
  state => ({}))
export default class LanguageForm extends Component {

  static propTypes = {
    setLanguage: PropTypes.func,
    fields: PropTypes.object,
    languageOptions: PropTypes.array
  };

  makeOnChangeSelect = (field) => {
    const {setLanguage} = this.props;
    return (value) => {
      field.onChange(value);
      setLanguage(value.value);
    };
  }

  renderLocalizationOption = (option) => {
    return <div key={option.value} className="language-option"> <img className="language-image" src={`/flagIcons/flag_${option.value}.png`}/></div>
  }

  render() {
    const {fields: {language},  onSelectLanguage, languageOptions} = this.props;

    return (
      <form className="language-picker">
        <CustomSelect
          {...language}
          data={languageOptions}
          renderOption={this.renderLocalizationOption}
          renderValue={this.renderLocalizationOption}
          onChange={this.makeOnChangeSelect(language)}
          clearable={false}
          searchable={false}
          top
        />
      </form>
    );
  }
}

// <form>
//   <FormControl style={{width: '100px'}} componentClass="select" onChange={(e) => setLanguage(e.target.value)} value={language.chosenLanguage}>
//     {language.supportedLanguages.map((option) =>
//       <option value={option.value} key={option.id}>{option.label}</option>
//     )}
//   </FormControl>
// </form>
