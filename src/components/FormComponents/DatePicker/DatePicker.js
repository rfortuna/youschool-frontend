/**
 * Created by urbanmarovt on 10/02/16.
 */

import React, {Component, PropTypes} from 'react';
import DateTimeField from 'react-bootstrap-datetimepicker';
import styles from './DatePicker.scss';

export default class DatePicker extends Component {

  static propTypes = {
    labelClassName: PropTypes.string,
    wrapperClassName: PropTypes.string,
    label: PropTypes.string,
    onBlur: PropTypes.func,
    viewInput: PropTypes.string,
    dirty: PropTypes.bool,
    error: PropTypes.string,
    value: PropTypes.number,
    inputFormat: PropTypes.string,
    mode: PropTypes.string
  }

  render() {
    const {labelClassName, wrapperClassName, label, value, onBlur, viewInput, dirty, error, mode, inputFormat, ...props} = this.props;

    return (
      <div className={`${styles.marginBottom} row`}>
        <div className={labelClassName}>
          <label className={`${styles.label} ${dirty && error && styles.error}`}>{label || ''}</label>
        </div>
        <div className={wrapperClassName}>
          <DateTimeField
            inputProps={ {className: `${styles.inputProps} form-control ${dirty && error && styles.error}`} }
            defaultText = ""
            dateTime= {value}
            value = {value}
            viewInput = {viewInput || 'days'}
            onBlur = {() => onBlur(value)}
            mode = {mode}
            inputFormat={inputFormat || 'DD. MM. YYYY HH:mm'}
            {...props} />
          {dirty && error &&
          <div className={styles.error}>
            {error}
          </div>}
        </div>
      </div>
    );
  }
}
