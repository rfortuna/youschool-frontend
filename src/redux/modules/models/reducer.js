import { combineReducers } from 'redux';
import userReducer from './users/reducer';
import videosReducer from './videos/reducer';
import booksReducer from './books/reducer';
import plansReducer from './plans/reducer';
import commmentsReducer from './comments/reducer';
import countriesReducer from './countries/reducer';

export default combineReducers({
  users: userReducer,
  videos: videosReducer,
  books: booksReducer,
  plans: plansReducer,
  comments: commmentsReducer,
  countries: countriesReducer
});
