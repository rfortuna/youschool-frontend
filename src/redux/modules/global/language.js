/**
 * Created by rokfortuna on 4/21/16.
 */
import cookie from 'react-cookie';

const SET_LANGUAGE = 'youschool/global/language/SET_LANGUAGE';

const initialState = {
  supportedLanguages:
    [{id: 1, label: 'English', value:'en'},
    {id: 2, label: 'Swedish', value:'sv'}],
  chosenLanguage: 'en'
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SET_LANGUAGE:
      if (__CLIENT__) {
        cookie.save('selectedLanguage', action.data.language, { path: '/' });
      }
      return {
        ...state,
        chosenLanguage: action.data.language
      };
    default:
      return state;
  }
}

export function setLanguage(language) {
  return {
    type: SET_LANGUAGE,
    data: {language: language}
  };
}
