/**
 * Created by urbanmarovt on 26/04/16.
 */
/**
 * Created by urbanmarovt on 25/04/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import {reduxForm} from 'redux-form';
import {Row, Col, Button, FormGroup, ControlLabel, FormControl, HelpBlock} from 'react-bootstrap';
import {toastr} from 'react-redux-toastr';
import _ from 'lodash';

import {validateMobilePhone} from 'utils/validation';
import {load as loadPlan} from 'redux/modules/models/plans/details';
import {requestBuyCredits} from 'redux/modules/utils/payment';
import {translateFormErrors} from 'utils/localization';


import multiStrings from './strings';

function fetchDataDeferred(getState, dispatch) {
  return dispatch(loadPlan(getState().router.params.planId));
}

const validate = (values) => {

  const errors = {};

  if (!values.mobileNumber) {
    errors.mobileNumber = 'required';
  } else if(!validateMobilePhone(values.mobileNumber)) {
    errors.mobileNumber = 'invalidMobileNumber';
  }

  return errors;

};

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    plan: state.models.plans.details.data,
    chosenLanguage: state.global.language.chosenLanguage
  }),
  dispatch => bindActionCreators({...routerActions, requestBuyCredits}, dispatch)
)
@reduxForm({
  form: 'Payment',
  fields: ['mobileNumber'],
  validate: validate
})
export default class BuyCredits extends Component {

  static propTypes = {
    plan: PropTypes.object,
    chosenLanguage: PropTypes.string
  };

  handlePay = (mobileNumber) => {
    const {requestBuyCredits, params: {planId}, chosenLanguage} = this.props;

    const strings = multiStrings[chosenLanguage];

    requestBuyCredits(mobileNumber, planId).then((res) => {
      if(res && !res.error) {
        toastr.success(strings.toastrs.paymentRequestSuccess);
      } else {
        toastr.error(strings.toastrs.paymentRequestError);
      }
    });
  };

  render() {
    const {plan, fields: {mobileNumber}, errors, chosenLanguage} = this.props;

    const strings = multiStrings[chosenLanguage];
    const translatedErrors = translateFormErrors(errors, strings.errors);

    return (
      <div className="container payment-container">
        <img src="/swish-logo.png" className="pull-right" style={{height: 40}}/>
        <h5>{strings.swishPayment}</h5>
        <p>{strings.price}: <strong>{plan.amount} {plan.currency}</strong></p>
        <p>{strings.credits}: <strong>{plan.credits}</strong></p>
        <form>
          <FormGroup controlId="formValidationSuccess1" validationState={mobileNumber.touched && mobileNumber.error ? 'error' : null}>
            <ControlLabel>{strings.mobileNumber}</ControlLabel>
            <FormControl type="text" {...mobileNumber} placeholder={strings.mobileNumberPlaceholder}/>
            {mobileNumber.touched && mobileNumber.error && <HelpBlock>{translatedErrors.mobileNumber}</HelpBlock>}
          </FormGroup>
        </form>
        <Button className="button-primary full-width pt8 pb8" disabled={_.keys(errors).length !== 0} onClick={() => this.handlePay(mobileNumber.value)}>{strings.actions.requestPayment}</Button>
      </div>
    );
  }
}
