/**
 * Created by urbanmarovt on 4/24/16.
 */

import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {bindActionCreators} from 'redux';
import {FormControl, FormGroup, Button, ControlLabel, Form} from 'react-bootstrap';
import {Link} from 'react-router';
import _ from 'lodash';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';

import {loadOptions} from 'redux/modules/models/videos/list';
import {CustomSelect} from 'components';

import stringsOptions from './strings';


let loadTimeout;

@connect(
  state => ({
    chosenLanguage: state.global.language.chosenLanguage,
    data: state.models.videos.list.selectOptions.data,
    loading: state.models.videos.list.selectOptions.loading,
  }),
  dispatch => bindActionCreators({...routerActions, loadOptions}, dispatch)
)
@reduxForm({
  form: 'VideoSearch',
  fields: ['videoName']
})
export default class VideoSearch extends Component {

  static propTypes = {
    onVideoClick: PropTypes.func,
    bookId: PropTypes.string,
    placeholder: PropTypes.string,
    style: PropTypes.object,
    handleVideoClickCustom: PropTypes.func
  };

  // renderVideoOption = (video) => {
  //   const {} = this.props;
  //
  //   return (<span> {`${video.name}, ${video.Book.name}, ${strings.page}: ${video.site}`} </span>);
  // }

  loadVideoOptions = (value) => {
    const {loadOptions, bookId} = this.props;
    let bookIdSearch = bookId ? bookId : '';
    if (loadTimeout) {
      clearTimeout(loadTimeout);
    }
    loadTimeout = setTimeout(() => {
      loadOptions(value, bookIdSearch);
    }, 300);
  }

  handleVideoClick = (item) => {
    const {pushState} = this.props;
    pushState(null, `/videos/${item.value.id}`);
  }

  render() {
    const {data, loading, fields: {videoName}, chosenLanguage, placeholder, style, handleVideoClickCustom} = this.props;
    const strings = stringsOptions[chosenLanguage];

    let handleVideoClick;
    let self = this;
    if (handleVideoClickCustom) {
      handleVideoClick = (item) => {
        self.handleVideoClick(item);
        handleVideoClickCustom(item);
      }
    } else {
      handleVideoClick = self.handleVideoClick;
    }

    return (
      <Form className="video-search" style={style}>
        <div>
          <CustomSelect
            {...videoName}
            data={data.map((item) => {return {value: item, label: `${item.name}, ${item.Book.name}, ${strings.page}: ${item.site}`};})}
            onInputChange={this.loadVideoOptions}
            onChange={handleVideoClick}
            isLoading={loading}
            placeholder={placeholder || ''}
          />
        </div>
      </Form>
    );
  }
}
