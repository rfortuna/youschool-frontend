/**
 * Created by urbanmarovt on 16/04/16.
 */

export default {
  en: {
    numberOfCredits: 'Number of credits',
    accountIsVerified: 'Account is verified',
    accountIsNotVerified: 'Account is not verified',
    firstName: "First name",
    lastName: "Last name",
    email: "Email",
    mobileNumber: "Mobile number",
    targetGrade: "Target grade",
    currentGrade: "Current grade",
    city: "City",
    country: "Country",
    school: "School",
    schoolClass: "School class",
    targetGrade: "Target grade",
    placeholders: {
      firstName: 'First name',
      lastName: 'Last name',
      email: 'Email',
      mobileNumber: 'Mobile number',
      password: 'Password',
      passwordCheck: 'Reenter password',
      school: 'School name',
      schoolClass: 'Class name',
      currentGrade: 'Current maths grade',
      targetGrade: 'Target maths grade',
      city: 'Home city',
      country: 'Home country'
    },
    errors: {
      required: 'Required.',
      invalidEmail: 'Email address is invalid.',
      passTooShort: 'Password is too short (8 characters at least).',
      passNotMatch: 'Passwords does not match.',
      invalidMobileNumber: 'Mobile number is invalid.',
      invalidInteger: 'Input should be a number.'
    },
    actions: {
      edit: 'Edit',
      update: 'Update',
      cancel: 'Cancel',
      verifyEmail: 'Verify email',
      buyCredits: 'Buy credits'
    },
    toastrs: {
      updateAccountSuccess: 'You have successfully updated your account.',
      verifyEmailSuccess: 'Check your email in order to verify your account.'
    }
  },
  sv: {
    numberOfCredits: 'swe_Number of credits',
    accountIsVerified: 'swe_Account is verified',
    accountIsNotVerified: 'swe_Account is not verified',
    firstName: "swe_First name",
    lastName: "swe_Last name",
    email: "swe_Email",
    mobileNumber: "swe_Mobile number",
    targetGrade: "swe_Target grade",
    currentGrade: "swe_Current grade",
    city: "swe_City",
    country: "swe_Country",
    school: "swe_School",
    schoolClass: "swe_School class",
    targetGrade: "swe_Target grade",
    placeholders: {
      firstName: 'swe_First name',
      lastName: 'swe_Last name',
      email: 'swe_Email',
      mobileNumber: 'swe_Mobile number',
      password: 'swe_Password',
      passwordCheck: 'swe_Reenter password',
      school: 'swe_School name',
      schoolClass: 'swe_Class name',
      currentGrade: 'swe_Current maths grade',
      targetGrade: 'swe_Target maths grade',
      city: 'swe_Home city',
      country: 'swe_Home country'
    },
    errors: {
      required: 'swe_Required.',
      invalidEmail: 'swe_Email address is invalid.',
      passTooShort: 'swe_Password is too short (8 characters at least).',
      passNotMatch: 'swe_Passwords does not match.',
      invalidMobileNumber: 'swe_Mobile number is invalid.',
      invalidInteger: 'swe_Input should be a number.'
    },
    actions: {
      edit: 'swe_Edit',
      update: 'swe_Update',
      cancel: 'swe_Cancel',
      verifyEmail: 'swe_Verify email',
      buyCredits: 'swe_Buy credits'
    },
    toastrs: {
      updateAccountSuccess: 'swe_You have successfully updated your account.',
      verifyEmailSuccess: 'swe_Check your email in order to verify your account.'
    }
  }
};
