import React, { Component, PropTypes } from 'react';
import config from '../../config';
import {Link} from 'react-router';
import Slider from 'react-slick';
import * as routerActions from 'redux-router';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import {reduxForm, reset as resetForm} from 'redux-form';
import {bindActionCreators} from 'redux';
import _ from 'lodash';
import {
  Button,
  Form,
  FormControl,
  FormGroup,
  ControlLabel,
  InputGroup,
  Glyphicon,
  Grid,
  Row,
  Col,
  Thumbnail
} from 'react-bootstrap';

import {
  onPageChange,
  onSearchStringChange,
  onOrderChange,
  setInitialState as setInitialStateQueryParams} from 'redux/modules/queryParams';
import {getQueryParamsHome, setQueryParams as setQueryParamsUrl} from 'utils/queryParams';
import {getOffset, getPagesCount, getActivePage} from 'utils/pagination';
import {load as loadVideos} from 'redux/modules/models/videos/list';
import {load as loadBooks, loadByTaskName as loadBooksByTaskName} from 'redux/modules/models/books/list';
import {Tag, SimpleSelectInline, Spinner} from 'components';
import {setDefaultBookProfile, setDefaultBookLocal, clearDefaultBookProfile} from 'redux/modules/global/profile';
import {trackEvent as trackEventMixpanel} from 'redux/modules/global/mixpanel';


import stringsOptions from './strings.js';

const defaultQueryParams = {
  count: 16,
  offset: 0
};

function fetchDataDeferred(getState, dispatch) {
  let promises = [];

  const queryParams = getQueryParamsHome(getState().router, defaultQueryParams);

  console.log('FETCH DATA');
  console.log(getState().global.profile.DefaultBookId);
  promises.push(dispatch(loadVideos(queryParams, getState().global.profile.DefaultBookId, queryParams.videoName)));
  promises.push(dispatch(loadBooks()));
  promises.push(dispatch(loadBooksByTaskName(queryParams.videoName ? queryParams.videoName : '')));

  // promises.push(dispatch(loadBooks()));

  return Promise.all(promises);
}

const readQueryParams = (router) => {
  return {};
};

let searchDelayTimeout;

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    videos: state.models.videos.list.data,
    books: state.models.books.list.data,
    booksByTaskName: state.models.books.list.booksByTaskName,
    profile: state.global.profile,
    chosenLanguage: state.global.language.chosenLanguage,
  }),
  dispatch => bindActionCreators(
    {...routerActions,
      onPageChange,
      onSearchStringChange,
      loadVideos,
      setDefaultBookProfile,
      setDefaultBookLocal,
      resetForm,
      loadBooksByTaskName,
      clearDefaultBookProfile,
      trackEventMixpanel
    }, dispatch)
)
@reduxForm({
    form: 'MainSearchForm',
    fields: ['videoName']
  },
  state => ({
    initialValues: readQueryParams(state.router)
  }))
export default class Home extends Component {

  static propTypes = {
    loadVideos: PropTypes.func,
    pushState: PropTypes.func,
    videos: PropTypes.array,
    profile: PropTypes.object,
    setDefaultBookProfile: PropTypes.func,
    setDefaultBookLocal: PropTypes.func,
    resetForm: PropTypes.func,
    chosenLanguage: PropTypes.string,
    loadBooksByTaskName: PropTypes.func
  };

  state = {
    loading: true
  }

  componentDidMount() {
    let self = this;
    setTimeout(() => {
      self.setState({loading: false});
    }, 500);
  }


  onFormStateChange = () => {
    if (searchDelayTimeout) {
      clearTimeout(searchDelayTimeout);
      searchDelayTimeout = null;
    }

    searchDelayTimeout = setTimeout(() => {
      const {loadVideos, fields: { videoName}, profile, loadBooksByTaskName} = this.props;
      loadVideos(defaultQueryParams, profile.DefaultBookId, videoName.value);
      loadBooksByTaskName(videoName.value);
    }, 300);

  };

  makeOnChangeInput = (field) => {
    return (event) => {
      field.onChange(event);
      this.onFormStateChange(field, event.target.value);
    };
  };

  makeOnChangeSelect = (field) => {
    return (value) => {
      field.onChange(value);
      this.onFormStateChange();
    };
  };

  handleVideoClick = (video) => {
    return () => {
      const {setDefaultBookProfile, setDefaultBookLocal, profile} = this.props;

      setDefaultBookLocal(video.Book);
      if (profile.id) {
        setDefaultBookProfile(video.BookId);
      }
      this.props.pushState(null, `/videos/${video.id}`);
    }
  }

  handleClearDefaultBook = () => {
    const {clearDefaultBookProfile, loadVideos} = this.props;
    clearDefaultBookProfile();
    loadVideos(defaultQueryParams, '');
  }

  render() {

    const {fields : {videoName},
      videos, setDefaultBookProfile, setDefaultBookLocal, books,
      profile, resetForm, chosenLanguage, booksByTaskName} = this.props;

    const strings = stringsOptions[chosenLanguage];

    var settings = {
      dots: true,
      infinite: false,
      autoplay: false,
      slidesToShow: booksByTaskName.length < 3 ?  booksByTaskName.length  : 3,
      slidesToScroll: 1,
      arrows: false,
      responsive: [{
          breakpoint: 1024,
          settings: {
              slidesToShow: booksByTaskName.length < 3 ?  booksByTaskName.length  : 3,
              slidesToScroll: 1,
          }
      },
      {
          breakpoint: 800,
          settings: {
              slidesToShow: booksByTaskName.length < 3 ?  booksByTaskName.length  : 3,
              slidesToScroll: 1
          }
      },
      {
          breakpoint: 600,
          settings: {
              slidesToShow: booksByTaskName.length < 3 ?  booksByTaskName.length  : 3,
              slidesToScroll: 1
          }
      },
      {
          breakpoint: 480,
          settings: {
              slidesToShow: booksByTaskName.length < 2 ?  booksByTaskName.length  : 2,
              slidesToScroll: 1
          }
      }]
    };

    let defaultBook = profile.DefaultBook;

    return (
      <div className="home padding-top-container text-center">

        {this.state.loading && <Spinner />}
        {!this.state.loading &&
          <div>
            <div className={`books mt40 ${profile.DefaultBookId ? '' : 'mb56'}`}>
              { !profile.DefaultBookId &&
              <Slider {...settings}>
                {booksByTaskName
                  .map((book) =>
                  <div key={book.id}>
                    <img src={book.photo} style={{width:'100px', margin: '0 auto'}} alt={book.name} />
                  </div>
                )}
              </Slider>
              }
              { profile.DefaultBookId &&
                <div className="text-center">
                  <div>
                    <img className="mb8" src={ defaultBook && defaultBook.photo || ''  } style={{width:'100px', margin: '0 auto'}} alt={defaultBook ? defaultBook.name : ''}/>
                  </div>
                  <div>
                    <Button  className="pull-left full-width button-primary clear-book" onClick={this.handleClearDefaultBook}>
                      Clear me
                    </Button>
                  </div>
                </div>
              }
            </div>
            <div className="text-center mt24 mb8">
              <Form inline>
                <FormGroup className="search-form" style={{width: '300px', margin: '0 auto'}}>
                  <FormControl type="text" placeholder={strings.videoNamePlaceholder} {...videoName} onChange={this.makeOnChangeInput(videoName)} />
                  <span className="icon-search"> </span>
                </FormGroup>
              </Form>
            </div>

            <Grid>
              <Row>
                {videos
                /*TODO -refactor prevent from rendering all videos the first time (we dont have the chosen BookId yet). */
                  // .filter((video) => {
                  //   if (!books[defaultBookIndex]) {
                  //     return true;
                  //   }
                  //   return video.BookId == books[defaultBookIndex].id;
                  // })
                  .map(
                  (video) =>
                    <Col xs={12} sm= {6} md={6} lg={4} className="video-card text-center">
                      <div className="video-image-container" onClick={this.handleVideoClick(video)}>
                        <span className="video-image-overlay"/>
                        <img className="video-image" src={video.imageUrl ? video.imageUrl : 'http://placehold.it/350x200'} />
                      </div>
                      <div className="text-center mt8">
                        <div className="text-size-sm">{video.name}</div>
                        <div className="text-size-xs">{`${video.Book.name}, ${strings.page} ${video.site}`}</div>
                      </div>
                    </Col>
                )}
              </Row>
            </Grid>
          </div>
        }
      </div>
    );
  }
}
//
// <Thumbnail>
//   {/*<video style={{height: 'px', width: '200px'}} controls>
//     <source src={video.url} type="video/ogg"/>
//   </video>*/}
//   <h3>{video.name}</h3>
//   <p>{video.description}</p>
//   <p>
//     Difficulty: &nbsp; <Tag tag={{value: video.Difficulty.type ,color: video.Difficulty.type}} />
//   </p>
//   <p>
//     <Button bsStyle="primary" >Watch</Button>
//   </p>
// </Thumbnail>
