/**
 * Created by rokfortuna on 1/08/16.
 */

import React, {Component, PropTypes} from 'react';
import Select from 'react-select';

import styles from './Selects.scss';

export default class CustomSelect extends Component {

  static propTypes = {
    data: PropTypes.array,
    onBlur: PropTypes.func,
    touched: PropTypes.bool,
    error: PropTypes.string,
    placeholder: PropTypes.string,
    renderOption: PropTypes.func,
    renderValue: PropTypes.func,
    top: PropTypes.bool,
    handleOnChange: PropTypes.func
  };

  render() {
    const {placeholder, onChange, data, value, onBlur, error, touched, handleOnChange, renderOption, renderValue, top, ...props} = this.props;

    let selectOnChange;
    if(handleOnChange) {
      selectOnChange = (value) => {
        this.setState({value});
        onChange(value);
        handleOnChange(value);
      }
    } else {
      selectOnChange = (value) => {
        this.setState({value});
        onChange(value);
      }
    }

    return (
          <Select
            {...props}
            className={`${styles.select} ${top ? 'Select-menu-top' : ''}`}
            onChange={selectOnChange}
            value={value}
            onBlur={() => onBlur(value)}
            placeholder={placeholder || ''}
            options = {data}
            optionRenderer={renderOption}
            valueRenderer={renderValue}
          />
    );
  }
}
