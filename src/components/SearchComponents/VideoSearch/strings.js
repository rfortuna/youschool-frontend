/**
 * Created by urbanmarovt on 24/04/16.
 */

export default {
  en: {
    bookPlaceholder: 'Choose book',
    exercisePlaceholder: 'Enter exercise'
  },
  sv: {
    bookPlaceholder: 'Choose book',
    exercisePlaceholder: 'Enter exercise'
  }
};
