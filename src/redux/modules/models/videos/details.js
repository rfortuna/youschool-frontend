/**
 * Created by urbanmarovt on 24/04/16.
 */

const LOAD = 'youschool/video/details/LOAD';
const LOAD_SUCCESS = 'youschool/video/details/LOAD_SUCCESS';
const LOAD_FAIL = 'youschool/video/details/LOAD_FAIL';
const LOAD_URL = 'youschool/video/details/LOAD_URL';
const LOAD_URL_SUCCESS = 'youschool/video/details/LOAD_URL_SUCCESS';
const LOAD_URL_FAIL = 'youschool/video/details/LOAD_URL_FAIL';
const PURCHASE = 'youschool/video/details/PURCHASE';
const PURCHASE_SUCCESS = 'youschool/video/details/PURCHASE_SUCCESS';
const PURCHASE_FAIL = 'youschool/video/details/PURCHASE_FAIL';

const CLEAR_STATE = 'youschool/video/details/CLEAR_STATE';

const initialState = {
  loading: false,
  loadingUrl: false,
  purchasingVideo: false,
  loaded: false,
  error: {},
  data: {
    Difficulty: {},
    Book: {},
    UserCommentedVideos: [],
    RelatedVideos: []
  },
  url: null,
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        data: initialState.data,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.body,
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: initialState.data,
        error: action.error
      };
    case LOAD_URL:
      return {
        ...state,
        url: null,
        loadingUrl: true
      };
    case LOAD_URL_SUCCESS:
      return {
        ...state,
        loadingUrl: false,
        url: action.result.body.Video.url
      };
    case LOAD_URL_FAIL:
      return {
        ...state,
        loadingUrl: false,
        url: null
      };
    case PURCHASE:
      return {
        ...state,
        url: null,
        purchasingVideo: true
      };
    case PURCHASE_SUCCESS:
      return {
        ...state,
        purchasingVideo: false,
        url: action.result.body.Video.url
      };
    case PURCHASE_FAIL:
      return {
        ...state,
        purchasingVideo: false,
        url: null
      };
    case CLEAR_STATE:
      return initialState;
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.models.videos.list && globalState.models.videos.list.loaded;
}

// put globalState as parameters
export function load(videoId) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token) => client.get(`/videos/${videoId}`, {token: token})
  };
}

export function loadUrl(videoId) {
  return {
    types: [LOAD_URL, LOAD_URL_SUCCESS, LOAD_URL_FAIL],
    promise: (client, token) => client.get(`/videos/${videoId}/url`, {token: token})
  }
}

export function purchase(videoId) {
  return {
    types: [PURCHASE, PURCHASE_SUCCESS, PURCHASE_FAIL],
    promise: (client, token) => client.post(`/videos/${videoId}/purchase`, {token: token})
  }
}

export function clear() {
  return {
    type: CLEAR_STATE
  }
}
