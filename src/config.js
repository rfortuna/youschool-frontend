require('babel/polyfill');

const environment = {
  development: {
    isProduction: false
  },
  production: {
    isProduction: true
  }
}[process.env.NODE_ENV || 'development'];

module.exports = Object.assign({
  host: process.env.HOST || 'localhost',
  port: process.env.PORT,
  apiHost: process.env.APIHOST || 'localhost',
  apiPort: process.env.APIPORT || '3030',
  mixpanelToken: '9a5c28b0f48643580dc5293e83964573',
  app: {
    title: 'YouSchool',
    description: 'YouSchool app.',
    meta: {
      charSet: 'utf-8',
      property: {
        'og:site_name': 'YouSchool',
        'og:image': 'https://react-redux.herokuapp.com/logo.jpg',
        'og:locale': 'en_US',
        'og:title': 'YouSchool',
        'og:description': 'YouSchool math courses.',
      }
    }
  }
}, environment);
