/**
 * Created by rokfortuna on 4/26/16.
 */

import React, {Component, PropTypes} from 'react';
import {Button, Modal, Row, Col, Glyphicon, FormControl,
  FormGroup, ControlLabel, HelpBlock} from 'react-bootstrap';
import _ from 'lodash';
import {connect} from 'react-redux';
import {reduxForm, getValues} from 'redux-form';
import {bindActionCreators} from 'redux';
import * as routerActions from 'redux-router';
import { Link } from 'react-router';
import {toastr} from 'react-redux-toastr';

import {hideLoginModal } from 'redux/modules/views/navigation';
import { validateEmail, validateMobilePhone } from 'utils/validation';
import {translateFormErrors} from 'utils/localization';
import {CheckboxYS} from 'components';
import {login, setRememberMe} from 'redux/modules/authentication';
import {load as loadProfileData} from 'redux/modules/global/profile';
import {trackEvent as trackEventMixpanel} from 'redux/modules/global/mixpanel';

import stringsOptions from './strings';

const validate = (values) => {

  const errors = {};

  if (!values.email) {
    errors.email = 'required';
  } else if (!validateEmail(values.email)) {
    errors.email = 'invalidEmail';
    //Invalid email address.
  }

  if (!values.password) {
    errors.password = 'required';
  }

  return errors;

};

@connect(
  state => ({
    chosenLanguage: state.global.language.chosenLanguage,
    visible: state.views.navigation.loginModalVisible,
    loginForm: state.form.LoginForm,
    loggingIn: state.authentication.loggingIn
  }),
  dispatch => bindActionCreators(
    {...routerActions,
      hideLoginModal,
      login,
      loadProfileData,
      setRememberMe,
      trackEventMixpanel
    }, dispatch)
)
@reduxForm({
  form: 'LoginForm',
  fields: ['email', 'password', 'rememberMe'],
  validate: validate
})
export default class OverflowLogin extends Component {

  static propTypes = {
    chosenLanguage: PropTypes.string,
    handleSubmit: PropTypes.func,
    visible: PropTypes.bool,
    hideLoginModal: PropTypes.func
  };

  handleSubmit = () => {
    const {login, loginForm, hideLoginModal, loadProfileData, chosenLanguage, trackEventMixpanel} = this.props;
    // console.log(loginForm);
    let data = getValues(loginForm);

    const strings = stringsOptions[chosenLanguage];

    login(data.email, data.password).then((res) => {
      if (res.error) {
        toastr.error(strings.invalidCredentials);
        console.log(`ERROR! ${res.error.error}`);
      } else {
        trackEventMixpanel("login", {email: data.email});
        toastr.success(strings.success);
        loadProfileData();
        hideLoginModal();
      }
    });
  }

  render() {
    const {visible, handleSubmit, handleCancel, chosenLanguage,
      hideLoginModal, fields: { email, password, passwordCheck, rememberMe}, errors, loggingIn, setRememberMe} = this.props;

    const strings = stringsOptions[chosenLanguage];
    const translatedErrors = translateFormErrors(errors, strings.errors);

    let self = this;

    return (
      <Modal show={visible} onHide={hideLoginModal} bsSize="small" className="modal modal-login">
        <Modal.Header closeButton>
          <Modal.Title>{strings.login}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Row>
            <Col xs={12} >
              <FormGroup validationState={ errors.email && email.touched ? 'error' : null}>
                <FormControl type="text" placeholder={strings.placeholders.email} {...email} />
                {email.touched && errors.email && <HelpBlock>{translatedErrors.email}</HelpBlock>}
              </FormGroup>
            </Col>
            <Col xs={12}>
              <FormGroup validationState={ errors.password && password.touched ? 'error' : null}>
                <FormControl type="password" placeholder={strings.placeholders.password} {...password} />
                {password.touched && errors.password && <HelpBlock>{translatedErrors.password}</HelpBlock>}
              </FormGroup>
            </Col>
            <Col xs={12} style={{ height: '35px'}}>
              <span className="remember-label"> {strings.rememberMe} </span>
              <CheckboxYS {...rememberMe} handleChange={setRememberMe}/>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <Button disabled={_.keys(errors).length !== 0} className="pull-left full-width button-primary" onClick={this.handleSubmit}>
                {(!loggingIn &&
                <span>{strings.login}</span>) ||
                  <span><i className="fa fa-spinner fa-spin"></i></span>}
              </Button>
            </Col>
          </Row>
          <Row style={{ height: '25px'}}>
            <Col xs={12}>
              <Link onClick={hideLoginModal} className="link" to="resetPassword">{strings.forgotPassword}</Link>
            </Col>
          </Row>
        </Modal.Body>
      </Modal>
    );
  }
}
