/**
 * Created by urbanmarovt on 14/04/16.
 */

import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {FormControl, FormGroup, Button, ControlLabel, HelpBlock} from 'react-bootstrap';
import {Link} from 'react-router';
import _ from 'lodash';
import strings from './strings';

import { validateEmail } from 'utils/validation';

const validate = (values) => {

  const errors = {};

  if (!values.firstName) {
    errors.firstName = 'required';
  }

  if (!values.lastName) {
    errors.lastName = 'required';
  }

  if (!values.email) {
    errors.email = 'required';
  }
  else if (!validateEmail(values.email)) {
    errors.email = 'invalidEmail';
    //Invalid email address.
  }

  if (!values.password) {
    errors.password = 'required';
  } else if (values.password.length < 8) {
    errors.password = 'passTooShort';
  }

  return errors;

};

@reduxForm({
  form: 'RegisterForm',
  fields: ['firstName', 'lastName', 'email', 'password'],
  validate: validate
})
export default class RegisterForm extends Component {

  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    authenticationData: PropTypes.object,
    fields: PropTypes.object,
    errors: PropTypes.object,
    chosenLanguage: PropTypes.string
  }

  render() {
    const {fields: {firstName, lastName, email, password}, errors, chosenLanguage} = this.props;

    const strings = strings[chosenLanguage];

    return (
      <form onSubmit={this.props.handleSubmit}>
        <FormGroup>
          <ControlLabel>{strings.labels.firstName}</ControlLabel>
          <FormControl type="text" placeholder={strings.placeholders.firstName} {...firstName} />
        </FormGroup>
        <FormGroup>
          <ControlLabel>{strings.labels.lastName}</ControlLabel>
          <FormControl type="text" placeholder={strings.placeholders.lastName} {...lastName} />
        </FormGroup>
        <FormGroup validationState={ errors.email && email.touched ? 'error' : null}>
          <ControlLabel>{strings.labels.email}</ControlLabel>
          <FormControl type="text" placeholder={strings.placeholders.email} {...email}/>
          {email.touched && errors.email &&
          <HelpBlock>{strings.errors[errors.email]}</HelpBlock>
            /*<div>{strings[errors.email]}</div>}*/
          }
        </FormGroup>
        <FormGroup validationState={ errors.password && password.touched ? 'error' : null}>
          <ControlLabel>{strings.labels.password}</ControlLabel>
          <FormControl type="password" placeholder={strings.placeholders.password} {...password} />
          {password.touched && errors.password && <HelpBlock>{strings.errors[errors.password]}</HelpBlock>}
        </FormGroup>
        <Button disabled={_.keys(errors).length !== 0} type="submit">{strings.createAccount}</Button>
        <p><small>{strings.youAlreadyHaveAccount}</small></p>
        <Link to="/login">{strings.login}</Link>
      </form>
    );
  }
}
