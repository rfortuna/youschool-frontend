/**
 * Created by rokfortuna on 4/26/16.
 */

export default {
  en: {
    wasVideoHelpful: 'Was this video helpful?',
    closeModal: 'Close',
    yes: 'Yes',
    no: 'No'
  },
  sv: {
    wasVideoHelpful: 'swe_Was this video helpful?',
    closeModal: 'swe_Close',
    yes: 'swe_Yes',
    no: 'swe_No'
  }
};