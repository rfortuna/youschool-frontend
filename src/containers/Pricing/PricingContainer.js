import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import connectData from 'helpers/connectData';
import {bindActionCreators} from 'redux';

import {load as loadPlans} from 'redux/modules/models/plans/list';
import {Pricing} from 'components';

function fetchDataDeferred(getState, dispatch) {
  let promises = [];

  promises.push(dispatch(loadPlans()));

  return Promise.all(promises);
}


@connectData(null, fetchDataDeferred)
@connect(
  state => ({
  }),
  dispatch => bindActionCreators(
    {}, dispatch)
)
export default class PricingContainer extends Component {

  static propTypes = {

  };

  render() {
    const {plans} = this.props;
    console.log(plans);

    return (
      <div className="pricing-container">
        <Pricing />
      </div>
    );
  }
}
