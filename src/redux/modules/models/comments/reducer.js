/**
 * Created by urbanmarovt on 10/08/16.
 */

import { combineReducers } from 'redux';

import commentsList from './list';
import commentsNew from './new';

export default combineReducers({
  new: commentsNew,
  list: commentsList
});
