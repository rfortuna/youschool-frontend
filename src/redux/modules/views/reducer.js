/**
 * Created by rokfortuna on 25/02/16.
 */

import { combineReducers } from 'redux';

import video from './video';
import navigation from './navigation';

export default combineReducers({
  video: video,
  navigation: navigation
});
