/**
 * Created by rokfortuna on 4/27/16.
 */

export default {
  sv : {
    placeholders: {
      firstName: 'First name',
      lastName: 'Last name',
      email: 'Email',
      mobileNumber: 'Mobile number',
      password: 'Password',
      passwordCheck: 'Reenter password',
      school: 'School name',
      schoolClass: 'Class name',
      currentGrade: 'Current maths grade',
      targetGrade: 'Target maths grade',
      city: 'Home city',
      country: 'Home country'
    },
    createAccount: 'Create account',
    youAlreadyHaveAccount: 'You already have an account?',
    login: 'Log In',
    errors: {
      required: 'Required.',
      invalidEmail: 'Email address is invalid.',
      passTooShort: 'Password is too short (8 characters at least).'
    }
  },
  en: {
    placeholders: {
      firstName: 'First name',
      lastName: 'Last name',
      email: 'Email',
      mobileNumber: 'Mobile number',
      password: 'Password',
      passwordCheck: 'Reenter password',
      school: 'School name',
      schoolClass: 'Class name',
      currentGrade: 'Current maths grade',
      targetGrade: 'Target maths grade',
      city: 'Home city',
      country: 'Home country'
    },
    createAccount: 'Create account',
    youAlreadyHaveAccount: 'You already have an account?',
    login: 'Log In',
    errors: {
      required: 'Required.',
      invalidEmail: 'Email address is invalid.',
      passTooShort: 'Password is too short (8 characters at least).'
    }
  }
};
