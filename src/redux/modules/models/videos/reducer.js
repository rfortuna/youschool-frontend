/**
 * Created by rokfortuna on 4/23/16.
 */

import { combineReducers } from 'redux';

import videosList from './list';
import videoDetails from './details';
import videoFeedback from './feedback';
import videoConsecutive from './consecutive';

export default combineReducers({
  list: videosList,
  details: videoDetails,
  feedback: videoFeedback,
  consecutive: videoConsecutive
});
