/**
 * Created by rokfortuna on 3/24/16.
 */

/**
 * Created by urbanmarovt on 01/03/16.
 */
import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {FormControl, FormGroup, Button, ControlLabel} from 'react-bootstrap';
import {Link} from 'react-router';
import _ from 'lodash';

import strings from './strings';

const validate = (values) => {

  const errors = {};

  if (!values.username) {
    errors.username = 'Required';
  }

  if (!values.password) {
    errors.password = 'Required';
  }

  return errors;

};

@reduxForm({
  form: 'LoginForm',
  fields: ['username', 'password'],
  validate: validate
})
export default class LoginForm extends Component {

  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    fields: PropTypes.object,
    errors: PropTypes.object,
    chosenLanguage: PropTypes.string
  };

  render() {
    const {fields: {username, password}, errors, chosenLanguage} = this.props;

    const stringsChosen = strings[chosenLanguage];

    return (
      <form onSubmit={this.props.handleSubmit}>
        <FormGroup>
          <ControlLabel>Email</ControlLabel>
          <FormControl type="text" placeholder={stringsChosen.emailPlaceholder} {...username} />
        </FormGroup>
        <FormGroup>
          <ControlLabel>Password</ControlLabel>
          <FormControl type="password" placeholder={stringsChosen.passwordPlaceholder} {...password} />
        </FormGroup>
        <Button disabled={_.keys(errors).length !== 0} type="submit">{stringsChosen.login}</Button>
        <Link to="/resetPassword"><small>{stringsChosen.forgotPassword}</small></Link>
        <p><small>{stringsChosen.dontHaveAccount}</small></p>
        <Link to="/register">{stringsChosen.register}</Link>
      </form>
    );
  }
}
