/**
 * Created by rokfortuna on 4/16/16.
 */

  import config from './../config';

export const trackEvent = (event, attributes) => {
  if (__CLIENT__) {
    console.log(`MIXPANEL EVENT: ${event}`);
    let mixpanel =  require('mixpanel-browser');
    mixpanel.init(config.mixpanelToken);
    mixpanel.track('Login', attributes);
  }
}

export const identifyUser = (email) => {
  if (__CLIENT__) {
    console.log(`MIXPANEL IDENTIFY: ${event}`);
    let mixpanel =  require('mixpanel-browser');
    mixpanel.identify(email);
  }
}