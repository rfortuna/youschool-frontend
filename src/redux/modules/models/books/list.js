
/**
 * Created by rokfortuna on 4/23/16.
 */

const LOAD = 'youschool/books/LOAD';
const LOAD_SUCCESS = 'youschool/books/LOAD_SUCCESS';
const LOAD_FAIL = 'youschool/books/LOAD_FAIL';

const LOAD_BY_TASK = 'youschool/books/LOAD_BY_TASK';
const LOAD_BY_TASK_SUCCESS = 'youschool/books/LOAD_BY_TASK_SUCCESS';
const LOAD_BY_TASK_FAIL = 'youschool/books/LOAD_BY_TASK_FAIL';

const initialState = {
  loaded: false,
  loading: false,
  editing: {},
  error: {},
  data: [],
  count: 0,
  booksByTaskName: []
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        data: [],
        loading: true
      };
    case LOAD_SUCCESS:
      const newCount = action.result.headers['content-range'].split('/')[1];
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.body,
        // typeof newCount == 'undefined' ? state.count : parseInt(newCount)
        count: parseInt(newCount),
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: [],
        count: 0,
        error: action.error
      };
      case LOAD_BY_TASK:
        return {
          ...state,
        };
      case LOAD_BY_TASK_SUCCESS:
        return {
          ...state,
          booksByTaskName: action.result.body
        };
      case LOAD_BY_TASK_FAIL:
        return {
          ...state,
          booksByTaskName: []
        };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.models.books.list && globalState.models.books.list.loaded;
}

// put globalState as parameters
export function load() {

  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token) => client.get(`/books`, { token: token})
  };
}

export function loadByTaskName(taskName) {

  const params = {
    taskName: taskName
  };

  return {
    types: [LOAD_BY_TASK, LOAD_BY_TASK_SUCCESS, LOAD_BY_TASK_FAIL],
    promise: (client, token) => client.get(`/books/byTaskName`,
      {token: token, params: params })
  };
}
