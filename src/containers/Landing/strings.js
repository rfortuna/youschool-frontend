export default {
  sv : {
    main: {
      pickUpLine: 'swe_Personalised approach to learning maths',
      pickUpLineSmall: 'swe_Instant solution videos. On demand 24/7 help. Personality based tutoring.',
      videoNamePlaceholder: 'swe_Search solution video by textbook exercise number',
      page: 'swe_Page',
      helpMeNow: 'swe_Help me now',
      findMoreVideos: 'swe_Find more videos',
      learnMore: 'swe_Learn more'
    },
    improvement: {
      improvementGuaranteed: 'Improvement guaranteed',
      hint: 'We helped hundreds of A Level students improve their maths score</br>and tackle tests with confidence',
      instantVideos: {
        num: '5,000+',
        title: 'instant video solutions',
        text: '100% coverage of the school maths curriculum'
      },
      students: {
        num: '250',
        title: 'students',
        text: 'Successfully tutored'
      },
      hours: {
        num: '7,500',
        title: 'hours',
        text: 'Tutoring lessons delivered'
      },
      studentsImproved: {
        num: '90%',
        title: 'of students',
        text: 'Improved their grade'
      },
      studentsPassed: {
        num: '100%',
        title: 'of students',
        text: 'Passed the course'
      }
    },
    videos: {
      textbookInVideos: 'Your A Level textbook in videos',
      videoEffectivness: 'Research indicates that videos are 3x more effective way of learning than text only solutions.',
      youschoolVideos: 'YouSchool solution videos provide an instant step by step demonstration for every single excercise in your maths textbook.',
      searchPlaceholder: 'Search solution video',
      textbooksCovered: 'Textbooks covered'
    },
    onDemand: {
      onDemandChat: 'On demand 24/7 chat',
      text: 'We understand how frustrating it can be to be stuck on a specific problem. That’s why we work on your timeline. Ask us your question (or include a photo) via instant message and get back step by step instructions and/or a photo/video of how to solve the problem.',
      noMoreWaiting: 'No more waiting. Raise your hand.',
      helpMeNow: 'Help me now'
    },
    teachers: {
      title: 'The right teacher for you',
      yearsOfExperience: 'Our tutors are teachers with years of experience. They know maths and they know how to teach it effectively.',
      notOnlyNumbers: 'We understand that maths is not only numbers. We match your tutor’s style to your learning style. Take a personality test to understand how you are wired to learn maths.',
      notJustBusiness: 'This is not just business. This is personal.'
    },
    fredrik: {
      title: 'Hello. I am Fredrik.',
      desc1: 'I am a teacher and a maths enthusiast. But I wasn’t always. My early struggles with maths led me to discover a process that makes maths reachable for anyone. It helped me conquer maths, and as a result I founded YouSchool.',
      desc2: 'Learning maths is best through a combination of instantly available solution videos and personalised tutoring lessons, especially in groups where students interact with and learn from each other.',
      desc3: 'I understand that each student is different and I treat each student differently. I adapt my teaching style to how the student best learns. As a result, I have been 100% successful at helping my students reach their goals. Not only they bumped their grade or passed the exam, they also gained confidence.',
      end: 'You can too. YouSchool and I are here to help you.'
    },
    methods: {
      proven: 'Proven and simple teaching methodology',
      penAndPaper: 'Pen and paper. All of our students and teachers love it.',
      text: 'In the days of technology overflow, pen and paper remains the most effective way of learning. It’s fast, it’s simpe , and you can take tit anywhere. We have developed a proprietary method of online teaching that allows a student to use simple pen and paper with YouSchool video cameras and software.'
    },
    studentsAndTeachers: {
      loveUs: 'Students and teachers love us',
      desc1: 'Our tutors are teachers with years of experience. They know maths and they know how to teach it effectively.',
      desc2: 'We understand that maths is not only numbers. We match your tutor’s style to your learning style. Take a personality test to understand how you are wired to learn maths.',
      end: 'This is not just business. This is personal.'
    },
    footer: {
      social: 'social',
      menu: {
        title: 'menu',
        home: 'Home',
        results: 'Results',
        solutionVideos: 'Solution videos',
        onDemand: 'On demand 24/7 chat',
        tutors: 'Tutors',
        pricing: 'Pricing',
        terms: 'Terms of Use'
      },
      account: {
        title: 'Account',
        login: 'Login',
        register: 'Register'
      },
      contact: {
        title: 'Contact',
        general: 'General inquiries',
        carreers: 'Carreers',
        address: {
          title: 'Adrress:',
          name: 'EdSpace',
          street: 'Block D, Hackney Community College',
          province: 'Falkirk St, London N1 6HQ',
          country: 'United Kingdom'
        }
      },
      rights: 'YouSchool Limited 2016. All right reserved.'
    },
  },
  en: {
    main: {
      pickUpLine: 'Personalised approach to learning maths',
      pickUpLineSmall: 'Instant solution videos. On demand 24/7 help. Personality based tutoring.',
      videoNamePlaceholder: 'Search solution video by textbook exercise number',
      page: 'Page',
      helpMeNow: 'Help me now',
      findMoreVideos: 'Find more videos',
      learnMore: 'Learn more'
    },
    improvement: {
      improvementGuaranteed: 'Improvement guaranteed',
      hint: 'We helped hundreds of A Level students improve their maths score</br>and tackle tests with confidence',
      instantVideos: {
        num: '5,000+',
        title: 'instant video solutions',
        text: '100% coverage of the school maths curriculum'
      },
      students: {
        num: '250',
        title: 'students',
        text: 'Successfully tutored'
      },
      hours: {
        num: '7,500',
        title: 'hours',
        text: 'Tutoring lessons delivered'
      },
      studentsImproved: {
        num: '90%',
        title: 'of students',
        text: 'Improved their grade'
      },
      studentsPassed: {
        num: '100%',
        title: 'of students',
        text: 'Passed the course'
      }
    },
    videos: {
      textbookInVideos: 'Your A Level textbook in videos',
      videoEffectivness: 'Research indicates that videos are 3x more effective way of learning than text only solutions.',
      youschoolVideos: 'YouSchool solution videos provide an instant step by step demonstration for every single excercise in your maths textbook.',
      searchPlaceholder: 'Search solution video',
      textbooksCovered: 'Textbooks covered'
    },
    onDemand: {
      onDemandChat: 'On demand 24/7 chat',
      text: 'We understand how frustrating it can be to be stuck on a specific problem. That’s why we work on your timeline. Ask us your question (or include a photo) via instant message and get back step by step instructions and/or a photo/video of how to solve the problem.',
      noMoreWaiting: 'No more waiting. Raise your hand.',
      helpMeNow: 'Help me now'
    },
    teachers: {
      title: 'The right teacher for you',
      yearsOfExperience: 'Our tutors are teachers with years of experience. They know maths and they know how to teach it effectively.',
      notOnlyNumbers: 'We understand that maths is not only numbers. We match your tutor’s style to your learning style. Take a personality test to understand how you are wired to learn maths.',
      notJustBusiness: 'This is not just business. This is personal.'
    },
    fredrik: {
      title: 'Hello. I am Fredrik.',
      desc1: 'I am a teacher and a maths enthusiast. But I wasn’t always. My early struggles with maths led me to discover a process that makes maths reachable for anyone. It helped me conquer maths, and as a result I founded YouSchool.',
      desc2: 'Learning maths is best through a combination of instantly available solution videos and personalised tutoring lessons, especially in groups where students interact with and learn from each other.',
      desc3: 'I understand that each student is different and I treat each student differently. I adapt my teaching style to how the student best learns. As a result, I have been 100% successful at helping my students reach their goals. Not only they bumped their grade or passed the exam, they also gained confidence.',
      end: 'You can too. YouSchool and I are here to help you.'
    },
    methods: {
      proven: 'Proven and simple teaching methodology',
      penAndPaper: 'Pen and paper. All of our students and teachers love it.',
      text: 'In the days of technology overflow, pen and paper remains the most effective way of learning. It’s fast, it’s simpe , and you can take tit anywhere. We have developed a proprietary method of online teaching that allows a student to use simple pen and paper with YouSchool video cameras and software.'
    },
    studentsAndTeachers: {
      loveUs: 'Students and teachers love us',
      desc1: 'Our tutors are teachers with years of experience. They know maths and they know how to teach it effectively.',
      desc2: 'We understand that maths is not only numbers. We match your tutor’s style to your learning style. Take a personality test to understand how you are wired to learn maths.',
      end: 'This is not just business. This is personal.'
    },
    footer: {
      social: 'social',
      menu: {
        title: 'menu',
        home: 'Home',
        results: 'Results',
        solutionVideos: 'Solution videos',
        onDemand: 'On demand 24/7 chat',
        tutors: 'Tutors',
        pricing: 'Pricing',
        terms: 'Terms of Use'
      },
      account: {
        title: 'Account',
        login: 'Login',
        register: 'Register'
      },
      contact: {
        title: 'Contact',
        general: 'General inquiries',
        carreers: 'Carreers',
        address: {
          title: 'Adrress:',
          name: 'EdSpace',
          street: 'Block D, Hackney Community College',
          province: 'Falkirk St, London N1 6HQ',
          country: 'United Kingdom'
        }
      },
      rights: 'YouSchool Limited 2016. All right reserved.'
    },
  }
};
