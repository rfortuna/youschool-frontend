/**
 * Created by urbanmarovt on 16/04/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import {Form, FormGroup, FormControl, Button, Row, Col} from 'react-bootstrap';
import {Element, scroller, animateScroll as scroll} from 'react-scroll';
import {Link} from 'react-router';
import Slider from 'react-slick';
import { showRegisterModal, showLoginModal } from 'redux/modules/views/navigation';

import {load as loadPlans} from 'redux/modules/models/plans/list';
import multiStrings from './strings';
import {StatisticComponent, VideoSelect} from 'components';
import {Pricing} from 'components';
import {setDefaultBookProfile, setDefaultBookLocal} from 'redux/modules/global/profile';
import {load as loadBooks} from 'redux/modules/models/books/list';


function fetchDataDeferred(getState, dispatch) {
  let promises = [];

  promises.push(dispatch(loadPlans()));
  promises.push(dispatch(loadBooks()));
  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    chosenLanguage: state.global.language.chosenLanguage,
    books: state.models.books.list.data,
    profile: state.global.profile,
  }),
  dispatch => bindActionCreators({...routerActions, setDefaultBookProfile,
    setDefaultBookLocal, showRegisterModal, showLoginModal }, dispatch)
)
export default class Landing extends Component {

  static propTypes = {
    chosenLanguage: PropTypes.string
  };

  handleScroll = (section) => {
    scroller.scrollTo(section, {
      duration: 1000,
      delay: 100,
      smooth: true
    })
  }



  handleVideoSelect = (item) => {
    const {books, setDefaultBookLocal, setDefaultBookProfile, profile} = this.props;
    console.log(item);
    setDefaultBookLocal(item.value.Book);
    if (profile.id) {
      setDefaultBookProfile(item.value.Book.id);
    }
  }

  render() {
    const {user, chosenLanguage, books, profile, showRegisterModal, showLoginModal} = this.props;

    const strings = multiStrings[chosenLanguage];

    var settings = {
      dots: true,
      infinite: false,
      autoplay: false,
      slidesToShow: books.length < 3 ?  books.length  : 3,
      slidesToScroll: 1,
      arrows: false,
      responsive: [{
          breakpoint: 1024,
          settings: {
              slidesToShow: books.length < 3 ?  books.length  : 3,
              slidesToScroll: 1,
          }
      },
      {
          breakpoint: 800,
          settings: {
              slidesToShow: books.length < 3 ?  books.length  : 3,
              slidesToScroll: 1
          }
      },
      {
          breakpoint: 600,
          settings: {
              slidesToShow: books.length < 3 ?  books.length  : 3,
              slidesToScroll: 1
          }
      },
      {
          breakpoint: 480,
          settings: {
              slidesToShow: books.length < 2 ?  books.length  : 2,
              slidesToScroll: 1
          }
      }]
    };

    return (
      <div className="landing">
        <div className="main">
          <h1>{strings.main.pickUpLine}</h1>
          <h5>{strings.main.pickUpLineSmall}</h5>

          <div className="text-center mt24">
            <VideoSelect style={{margin: '0 auto'}} placeholder={strings.main.videoNamePlaceholder} handleVideoClickCustom={this.handleVideoSelect}/>
          </div>
          <div className="text-center">
            {profile.id &&
              <Button className="button-primary pt8 pb8 mt16 ml8 mr8 intercom-launcher" style={{width: '200px'}}>{strings.main.helpMeNow}</Button> ||
              <Button className="button-primary pt8 pb8 mr16 ml8 mr8 disabled" style={{width: '200px'}}>{strings.main.helpMeNow}</Button>
            }
            <Link to="/videos"><Button className="button-primary mt16 pt8 pb8" style={{width: '200px'}}>{strings.main.findMoreVideos}</Button></Link>
          </div>

          <div className="learn-more-container pb8 text-center clickable" onClick={() => this.handleScroll('improvement')}>
            <p className="mb0">{strings.main.learnMore}</p>
            <i className="fa fa-angle-down text-size-mlg"></i>
          </div>
        </div>
        <Element name="improvement">
          <div className="improvement container">
            <div className="pt48 pb32 text-center">
              <h4>{strings.improvement.improvementGuaranteed}</h4>
              <p>{strings.improvement.hint}</p>
            </div>
            <div className="statistics-container pt64">
              <Row>
                <Col xs={12} md={1}></Col>
                <Col xs={12} md={2} className="pb84"><StatisticComponent strings={strings.improvement.instantVideos}></StatisticComponent></Col>
                <Col xs={12} md={2} className="pb84"><StatisticComponent strings={strings.improvement.students}></StatisticComponent></Col>
                <Col xs={12} md={2} className="pb84"><StatisticComponent strings={strings.improvement.hours}></StatisticComponent></Col>
                <Col xs={12} md={2} className="pb84"><StatisticComponent strings={strings.improvement.studentsImproved}></StatisticComponent></Col>
                <Col xs={12} md={2} className="pb84"><StatisticComponent strings={strings.improvement.studentsPassed}></StatisticComponent></Col>
              </Row>
            </div>
          </div>
        </Element>
        <div className="cover-image student-with-ipad">

        </div>
        <Element name="videos">
          <div className="videos container">
            <Row style={{position: 'relative'}}>
              <Col xs={12} sm={6} className="pt32 pb32">
                <h4 className="pb16">{strings.videos.textbookInVideos}</h4>
                <p>{strings.videos.videoEffectivness}</p>
                <p className="pb64">{strings.videos.youschoolVideos}</p>
                <div className="pb32">
                  <VideoSelect placeholder={strings.videos.searchPlaceholder} handleVideoClickCustom={this.handleVideoSelect}/>
                </div>
                <h6>{strings.videos.textbooksCovered}</h6>
                <div className="images-container">
                  <Slider {...settings}>
                    {books
                      .map((book) =>
                      <div key={book.id}>
                        <img src={book.photo} style={{width:'100px', margin: '0 auto'}} alt={book.name} />
                      </div>
                    )}
                  </Slider>
                </div>
              </Col>
              <img src="/images/iphone.png" className="hidden-xs iphone"/>
            </Row>
          </div>
        </Element>
        <div className="cover-image hands">

        </div>
        <Element name="on-demand-chat">
          <div className="on-demand-chat">
            <div className="container">
              <Row style={{position: 'relative'}}>
                <img src="/images/iphone-intercom.png" className="hidden-xs iphone"/>
                <Col xs={12} smOffset={8} sm={4} className="text-right pt32 pb96">
                  <h4 className="pb16">{strings.onDemand.onDemandChat}</h4>
                  <p className="pb8">{strings.onDemand.text}</p>
                  <p className="pb16">{strings.onDemand.onMoreWaiting}</p>
                  {profile.id &&
                    <Button className="button-primary pt8 pb8 mr8 intercom-launcher" style={{width: '200px'}}>{strings.main.helpMeNow}</Button> ||
                    <Button className="button-primary pt8 pb8 mr8 disabled" style={{width: '200px'}}>{strings.main.helpMeNow}</Button>
                  }
                </Col>
              </Row>
            </div>
          </div>
        </Element>
        <Element name="teachers">
          <div className="teachers">
            <Row className="margin0">
              <Col xs={12} lg={4} className="pb32 pt32 teachers-container">
                <h4 className="pb16">{strings.teachers.title}</h4>
                <p className="pb8">{strings.teachers.yearsOfExperience}</p>
                <p className="pb8">{strings.teachers.notOnlyNumbers}</p>
                <p className="pb8">{strings.teachers.notJustBusiness}</p>
              </Col>
              <Col xs={12} lgOffset={1} lg={7} className="mt32 mb64 pr0 pl0 fredrik-container">
                <img className="pull-left mr8 hidden-xs" src="/images/Fredrik.jpg"/>
                <div className="description pt16 pl8 pr8 pb8">
                  <h5 className="pb8">{strings.fredrik.title}</h5>
                  <p className="pb8">{strings.fredrik.desc1}</p>
                  <p className="pb8">{strings.fredrik.desc2}</p>
                  <p className="pb8">{strings.fredrik.desc3}</p>
                  <p className="pb24">{strings.fredrik.end}</p>
                </div>
              </Col>
            </Row>
          </div>
        </Element>
        <div className="cover-image platform">

        </div>
        <Element name="methods">
          <div className="methods container">
            <Row className="pb32 pt32">
              <Col xs={12} sm={6} className="hidden-xs camera">
                <img src="/images/kamera-liten.jpg"/>
              </Col>
              <Col xs={12} sm={6} className="text-right">
                <h4 className="pb16">{strings.methods.proven}</h4>
                <p className="pb8">{strings.methods.penAndPaper}</p>
                <p className="pb8">{strings.methods.text}</p>
              </Col>
            </Row>
          </div>
          <div className="methods container">
            <Row className="pb32 pt64">
              <Col xs={12} sm={6}>
                <h4 className="pb16">{strings.studentsAndTeachers.loveUs}</h4>
                <p className="pb8">{strings.studentsAndTeachers.desc1}</p>
                <p className="pb8">{strings.studentsAndTeachers.desc2}</p>
                <p className="pb8">{strings.studentsAndTeachers.end}</p>
              </Col>
            </Row>
          </div>
        </Element>
        <Element name="pricing">
          <div className="pricing">
            <div className="container pt32">
              <Pricing />
            </div>
          </div>
        </Element>
        <div className="footer">
          <div className="container pt32">
            <Row>
              <Col xs={6} sm={5} className="pb32">
                <h6 className="text-uppercase pb24">{strings.footer.social}</h6>
                <div className="pb8">
                  <i className="fa fa-facebook social-icon"></i>
                </div>
                <div className="pb8">
                  <i className="fa fa-twitter social-icon"></i>
                </div>
                <div>
                  <i className="fa fa-linkedin social-icon"></i>
                </div>
              </Col>
              <Col xs={6} sm={2} className="pb32">
                <h6 className="text-uppercase pb24">{strings.footer.menu.title}</h6>
                <a className="pb8 font-xxs" onClick={() => scroll.scrollToTop()}>{strings.footer.menu.home}</a>
                <a className="pb8 font-xxs" onClick={() => this.handleScroll('improvement')}>{strings.footer.menu.results}</a>
                <a className="pb8 font-xxs" onClick={() => this.handleScroll('videos')}>{strings.footer.menu.solutionVideos}</a>
                <a className="pb8 font-xxs" onClick={() => this.handleScroll('on-demand-chat')}>{strings.footer.menu.onDemand}</a>
                <a className="pb8 font-xxs" onClick={() => this.handleScroll('teachers')}>{strings.footer.menu.tutors}</a>
                <a className="pb8 font-xxs" onClick={() => this.handleScroll('pricing')}>{strings.footer.menu.pricing}</a>
                <a className="pb8 font-xxs">{strings.footer.menu.terms}</a>
              </Col>
              <Col xs={6} sm={2} className="pb32">
                <h6 className="text-uppercase pb24">{strings.footer.account.title}</h6>
                {(profile.id &&
                  <Link to='/user'>{profile.firstName} {profile.lastName}</Link>) ||
                  <div>
                    <a className="pb8 font-xxs" onClick={showLoginModal}>{strings.footer.account.login}</a>
                    <a className="pb8 font-xxs" onClick={showRegisterModal}>{strings.footer.account.register}</a>
                  </div>
                }
              </Col>
              <Col xs={6} sm={3} className="pb32">
                <h6 className="text-uppercase pb24">{strings.footer.contact.title}</h6>
                <p className="font-xxs">{strings.footer.contact.general}</p>
                <p className="font-xxs">{strings.footer.contact.carreers}</p>
                <p className="font-xxs">{strings.footer.contact.address.title}</p>
                <p className="font-xxs">{strings.footer.contact.address.name}</p>
                <p className="font-xxs">{strings.footer.contact.address.street}</p>
                <p className="font-xxs">{strings.footer.contact.address.province}</p>
                <p className="font-xxs">{strings.footer.contact.address.country}</p>
                <p className="pt16 font-xxs color-gray-light">{strings.footer.rights}</p>
              </Col>
            </Row>
          </div>
        </div>
      </div>
    );
  }
}
