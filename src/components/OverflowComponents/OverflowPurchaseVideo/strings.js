/**
 * Created by urbanmarovt on 24/04/16.
 */

export default {
  en: {
    purchaseVideo: 'Purchase video',
    exerciseDifficulty: 'Exercise difficulty',
    numOfCredits: 'Number of credits',
    book: 'Book',
    actions: {
      purchaseVideo: 'Purchase video',
      cancel: 'Cancel'
    }
  },
  sv: {
    purchaseVideo: 'swe_Purchase video',
    exerciseDifficulty: 'swe_Exercise difficulty',
    numOfCredits: 'swe_Number of credits',
    book: 'swe_Book',
    actions: {
      purchaseVideo: 'swe_Purchase video',
      cancel: 'swe_Cancel'
    }
  }
};
