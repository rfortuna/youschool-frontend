/**
 * Created by rokfortuna on 4/16/16.
 */

import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {FormControl, FormGroup, Button, ControlLabel} from 'react-bootstrap';
import {Link} from 'react-router';
import _ from 'lodash';

const validate = (values) => {

  const errors = {};

  if (!values.firstName) {
    errors.username = 'Required';
  }

  if (!values.lastName) {
    errors.lastName = 'Required';
  }

  return errors;

};

@reduxForm({
  form: 'UserDataForm',
  fields: ['firstName', 'lastName', 'address', 'city', 'postalNumber', 'mobileNumber', 'school', 'school', 'schoolClass', 'targetGrade' ],
  validate: validate
})
export default class UserForm extends Component {

  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    fields: PropTypes.object,
    errors: PropTypes.object
  };

  render() {
    const {fields: {firstName, lastName, address, city, postalNumber, mobileNumber, school, schoolClass, targetGrade}, errors} = this.props;

    return (
      <form onSubmit={this.props.handleSubmit}>
        <FormGroup>
          <ControlLabel>First name</ControlLabel>
          <FormControl type="text" placeholder="Enter first name" {...firstName} />
        </FormGroup>
        <FormGroup>
          <ControlLabel>Last name</ControlLabel>
          <FormControl type="text" placeholder="Enter last name" {...lastName} />
        </FormGroup>
        <FormGroup>
          <ControlLabel>Address</ControlLabel>
          <FormControl type="text" placeholder="Enter address" {...address} />
        </FormGroup>
        <FormGroup>
          <ControlLabel>City</ControlLabel>
          <FormControl type="text" placeholder="Enter city" {...city} />
        </FormGroup>
        <FormGroup>
          <ControlLabel>Postal code</ControlLabel>
          <FormControl type="text" placeholder="Enter postal code" {...postalNumber} />
        </FormGroup>
        <FormGroup>
          <ControlLabel>Mobile</ControlLabel>
          <FormControl type="text" placeholder="Enter mobile number" {...mobileNumber} />
        </FormGroup>
        <FormGroup>
          <ControlLabel>School</ControlLabel>
          <FormControl type="text" placeholder="Enter school" {...school} />
        </FormGroup>
        <FormGroup>
          <ControlLabel>Class</ControlLabel>
          <FormControl type="text" placeholder="Enter class" {...schoolClass} />
        </FormGroup>
        <FormGroup>
          <ControlLabel>Target grade</ControlLabel>
          <FormControl type="text" placeholder="Enter target grade" {...targetGrade} />
        </FormGroup>
        <Button disabled={_.keys(errors).length !== 0} type="submit">Save</Button>
      </form>
    );
  }
}
