const LOAD = 'youschool/comments/LOAD';
const LOAD_SUCCESS = 'youschool/comments/LOAD_SUCCESS';
const LOAD_FAIL = 'youschool/comments/LOAD_FAIL';

const initialState = {
  loaded: false,
  error: {},
  data: [],
  count: 0
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        data: [],
        loading: true
      };
    case LOAD_SUCCESS:
      const newCount = action.result.headers['content-range'];
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.body,
        count: parseInt(newCount),
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: [],
        count: 0,
        error: action.error
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.models.videos.list && globalState.models.videos.list.loaded;
}

// put globalState as parameters
export function load(videoId, queryParams = {count: 100, offset: 0}) {

  const params = {
    ...queryParams
  };

  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token) => client.get(`videos/${videoId}/comments`, { params: params, token: token})
  };
}
