/**
 * Created by rokfortuna on 3/24/16.
 */
import cookie from 'react-cookie';

const SIGNUP = 'youschool/authentication/SIGNUP';
const AUTH_SUCCESS = 'youschool/authentication/AUTH_SUCCESS';
const SIGNUP_FAIL = 'youschool/authentication/SIGNUP_FAIL';

const LOGIN = 'youschool/authentication/LOGIN';
const LOGIN_FAIL = 'youschool/authentication/LOGIN_FAIL';
const LOGOUT = 'youschool/authentication/LOGOUT';

const FILL_USER_DATA_COOKIE = 'youschool/authentication/FILL_USER_DATA_COOKIE';

const REFRESH_TOKEN = 'youschool/authentication/REFRESH_TOKEN';
const REFRESH_TOKEN_SUCCESS = 'youschool/authentication/REFRESH_TOKEN_SUCCESS';
const REFRESH_TOKEN_FAIL = 'youschool/authentication/REFRESH_TOKEN_FAIL';

const SET_REMEMBER_ME = 'youschool/authentication/SET_REMEMBER_ME';

const initialState = {
  error: {},
  loggingIn: false,
  token: null,
  refreshToken: null,
  tokenExpiresAt: null,
  rememberMe: false
};

export default function reducer(state = initialState, action = {}) {
  const curTime = new Date().getTime();

  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        loggingIn: true,
      };
    case AUTH_SUCCESS:
      // if user wishes to be remembered
      // if (state.rememberMe) {
        cookie.save('auth_token', action.result.body.access_token, { path: '/' });
        cookie.save('refresh_token', action.result.body.refresh_token, { path: '/' });
        cookie.save('token_expires_at', action.result.body.expires_in, { path: '/' });
      // }
      return {
        ...state,
        loggingIn: false,
        token: action.result.body.access_token,
        refreshToken: action.result.body.refresh_token,
        tokenExpiresAt: action.result.body.expires_in
      };
    case LOGIN_FAIL:
      return {
        ...initialState,
        error: action.error
      };
    case SIGNUP:
      return {
        ...state,
        loggingIn: true,
      };
    case SIGNUP_FAIL:
      return {
        ...initialState,
        error: action.error,
      };
    case LOGOUT:
      console.log('LOGOUT REDUCER');
      cookie.remove('auth_token', { path: '/' });
      cookie.remove('refresh_token', { path: '/' });
      cookie.remove('token_expires_at', { path: '/' });
      return initialState;

    case FILL_USER_DATA_COOKIE:
      console.log('FILL_USER_DATA_COOKIE');
      return {
        ...state,
        token: action.data.token,
        refreshToken: action.data.refreshToken,
        tokenExpiresAt: action.data.tokenExpiresAt
      };
    case REFRESH_TOKEN:
      return {
        ...state,
        refreshingToken: true
      };
    case REFRESH_TOKEN_SUCCESS:
      cookie.save('auth_token', action.result.body.access_token, { path: '/' });
      cookie.save('refresh_token', action.result.body.refresh_token, { path: '/' });
      cookie.save('token_expires_at', action.result.body.expires_in, { path: '/' });

      return {
        ...state,
        refreshingToken: false,
        token: action.result.body.access_token,
        refreshToken: action.result.body.refresh_token,
        tokenExpiresAt: action.result.body.expires_in
      };
    case REFRESH_TOKEN_FAIL:

      cookie.remove('auth_token', {path: '/'});
      cookie.remove('refresh_token', {path: '/'});
      cookie.remove('token_expires_at', {path: '/'});

      return initialState;
    case SET_REMEMBER_ME:
      return {
        ...state,
        rememberMe: action.data.rememberMe
      }
    default:
      return state;
  }
}

export function login(email, password) {
  return {
    types: [LOGIN, AUTH_SUCCESS, LOGIN_FAIL],
    promise: (client) => client.post('/login/local', {
      formData: {
        email: email,
        password: password
      }
    })
  };
}

export function createAccount(data) {
  return {
    types: [SIGNUP, AUTH_SUCCESS, SIGNUP_FAIL],
    promise: (client) => client.post(`/users/createAccount`, {
      formData: data
    })
  };
}

export function refreshToken(refreshToken) {
  return {
    types: [REFRESH_TOKEN, REFRESH_TOKEN_SUCCESS, REFRESH_TOKEN_FAIL],
    promise: (client) => client.post(`/login/updateToken`, {
      formData: {
        refresh_token: refreshToken
      }
    })
  };
}

export function logout() {
  return {
    type: LOGOUT
  };
}

export function fillTokenCookie(token, refreshToken, tokenExpiresAt) {
  return {
    type: FILL_USER_DATA_COOKIE,
    data: {token: token, refreshToken: refreshToken, tokenExpiresAt: tokenExpiresAt}
  };
}


export function isLoaded(globalState){
  return globalState.authentication.token !== null;
}

export function setRememberMe(rememberMe){
  return {
    type: SET_REMEMBER_ME,
    data: {rememberMe: rememberMe}
  };
}
