/**
 * Created by urbanmarovt on 14/04/16.
 */

import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import connectData from 'helpers/connectData';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';

import {verifyEmailComfirm} from 'redux/modules/utils/verifyEmail';

import stringsOptions from './strings';

function fetchDataDeferred(getState, dispatch) {
  const promises = [];

  promises.push(dispatch(verifyEmailComfirm(getState().router.params.token)));

  return Promise.all(promises);
}

let countTimeout;

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    emailConfirmed: state.utils.verifyEmail.emailConfirmed,
    chosenLanguage: state.global.language.chosenLanguage,
  }),
  dispatch => bindActionCreators({...routerActions}, dispatch))
export default class VerifyEmail extends Component {

  static propTypes = {
    emailConfirmed: PropTypes.boolean
  };

  state = {
    countSeconds: 10
  }

  componentDidMount() {
    const {pushState} = this.props;
    this.setTimeoutCount();
    setTimeout( () => {
      clearTimeout(countTimeout);
      pushState(null, '/');
    }, 10000);
  }

  setTimeoutCount = () => {
    self = this;
    countTimeout = setTimeout(()=> {
      if (self.state.countSeconds > 0){
        self.setState({
          countSeconds: self.state.countSeconds - 1
        });
        self.setTimeoutCount();
      }
    }, 1000);
  }

  render() {
    const {emailConfirmed, pushState, chosenLanguage} = this.props;
    const strings = stringsOptions[chosenLanguage];

    return (

      <div className="verify-email-container mt96 mb96 pt96 pb48 text-center">
        {emailConfirmed &&
          <div>
            <h3>{strings.success}</h3>
            <h4>{strings.youWillBeRedirected(this.state.countSeconds)}</h4>
          </div>
        }
        {!emailConfirmed &&
        <div>
          <h2>{strings.tokenExpired}</h2>
          <h4>{strings.youWillBeRedirected(this.state.countSeconds)}</h4>
        </div>
        }
      </div>

    );
  }
}
