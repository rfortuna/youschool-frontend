/**
 * Created by urbanmarovt on 16/04/16.
 */
import { combineReducers } from 'redux';
import profileDetails from './details';
export default combineReducers({
  details: profileDetails
});
