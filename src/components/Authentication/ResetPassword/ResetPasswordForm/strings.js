export default {
  en: {
    errors: {
      required: 'Required.',
      passDontMatch: 'Passwords do not match.',
      passTooShort: 'Password too short'
    },
    password: 'Password',
    confirmPassword: 'Confirm password',
    enterEmailAddress: 'Enter Email address',
    resetPassword: 'Reset password',
    enterNewPassword: 'Enter new password',
    confirmNewPassword: 'Confirm new password'
  },
  sv: {
    errors: {
      required: 'swe_Required.',
      passDontMatch: 'swe_Passwords do not match.',
      passTooShort: 'swe_Password too short'
    },
    password: 'swe_Password',
    confirmPassword: 'swe_Confirm password',
    enterEmailAddress: 'swe_Enter Email address',
    resetPassword: 'swe_Reset password',
    enterNewPassword: 'swe_Enter new password',
    confirmNewPassword: 'swe_Confirm new password'

  }
}
