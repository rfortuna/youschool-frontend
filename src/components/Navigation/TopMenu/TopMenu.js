/**
 * Created by urbanmarovt on 22/02/16.
 */

import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Navbar, Input, NavItem, Nav, NavDropdown, MenuItem} from 'react-bootstrap';
import { Link } from 'react-router';
import _ from 'lodash';
import {reduxForm} from 'redux-form';
import * as routerActions from 'redux-router';

import {
  OverflowRegister,
  OverflowLogin
} from 'components';
import { showRegisterModal, showLoginModal } from 'redux/modules/views/navigation';
import { LanguageForm, VideoSelect } from 'components';
import stringsOptions from './strings';
import {clearDefaultBookProfile} from 'redux/modules/global/profile';

@connect(
  state => ({
    chosenLanguage: state.global.language.chosenLanguage,
    registerModalVisible: state.views.navigation.registerModalVisible,
    pathname: state.router.location.pathname
  }),
  dispatch => bindActionCreators({ ...routerActions, showRegisterModal, showLoginModal, clearDefaultBookProfile}, dispatch)
)
@reduxForm({
    form: 'TopMenuTaskSearch',
    fields: ['videoName']
  },
  state => ({

  }))
export default class TopMenu extends Component {

  static propTypes = {
    profile: PropTypes.object,
    logout: PropTypes.func,
    registerModalVisible: PropTypes.bool,
    showRegisterModal: PropTypes.func
  };

  clearDefaultBook = () => {
    const { clearDefaultBookProfile, pushState } = this.props;
    clearDefaultBookProfile();
    pushState(null, `/videos`);
  }

  linkToUser = () => {
    const { pushState } = this.props;

    pushState(null, '/user');
  }

  render() {
    const {logout, profile, pushState,
      chosenLanguage, showRegisterModal,
      registerModalVisible, showLoginModal,
      loginModalVisible, pathname, fields: {videoName}} = this.props;
      const strings = stringsOptions[chosenLanguage];

    return (
      <div className={pathname === '/' ? 'border-bottom top-menu landing-nav' : 'border-bottom top-menu'}>
        <OverflowRegister visible={registerModalVisible} />
        <OverflowLogin />
        <Navbar >
          <Navbar.Header>
            <Navbar.Brand onClick={() => pushState(null, "/")} className="clickable">
                <img className="brand" src="/images/ys_logo.png"/>
                <span>YouSchool</span>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
          { pathname.substring(0, 8) === '/videos/' &&
          <Nav pullLeft>
            <NavItem eventKey={1} className="top-search">
              <VideoSelect bookId={profile.DefaultBookId} />
            </NavItem>
            { profile.DefaultBook &&
            <NavItem eventKey={2} className="top-book-image">
              <img src={profile.DefaultBook.photo} onClick={this.clearDefaultBook}/>
            </NavItem>
            }
          </Nav>}
            {profile.id &&
            <Nav pullRight>
              <NavDropdown eventKey={1} title={`${profile.firstName} ${profile.lastName}`} id="basic-nav-dropdown">
                <MenuItem eventKey={1.1} onClick={this.linkToUser}>
                  <i className="fa fa-lg fa-user icon"/>&nbsp;&nbsp;{strings.profile}
                </MenuItem>
                <MenuItem eventKey={1.2} onClick={logout}>
                  <i className="fa fa-lg fa-sign-out icon"/>&nbsp;&nbsp;{strings.logout}
                </MenuItem>
              </NavDropdown>
              <NavItem eventKey={2} className="credits">
                <i className="fa fa-graduation-cap"> </i> &nbsp;{profile.credits}
              </NavItem>
              <NavItem eventKey={3}>
                <LanguageForm
                  initialValues={{language: chosenLanguage}}
                />
              </NavItem>
            </Nav>}
          {!profile.id &&
          <Nav pullRight>
            <NavItem eventKey={2} className="button white login text-uppercase" onClick={showLoginModal}>
              {strings.login}
            </NavItem>
            <NavItem eventKey={2} onClick={showRegisterModal} className="button white register text-uppercase text-center">
              {strings.register}
            </NavItem>
            <NavItem eventKey={3}>
              <LanguageForm
                initialValues={{language: chosenLanguage}}
              />
            </NavItem>
          </Nav>}
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
}
