/**
 * Created by urbanmarovt on 16/04/16.
 */

const LOAD = 'youschool/models/profile/LOAD';
const LOAD_SUCCESS = 'youschool/models/profile/LOAD_SUCCESS';
const LOAD_FAIL = 'youschool/models/profile/LOAD_FAIL';
const UPDATE = 'youschool/models/profile/UPDATE';
const UPDATE_SUCCESS = 'youschool/models/profile/UPDATE_SUCCESS';
const UPDATE_FAIL = 'youschool/models/profile/UPDATE_FAIL';
const CLEAR_STATE = 'youschool/models/profile/CLEAR_STATE';
const EDIT = 'youschool/models/profile/EDIT';
const EDIT_CANCEL = 'youschool/models/profile/EDIT_CANCEL';

const initialState = {
  data: {},
  loading: false,
  editing: false,
  updating: false,
  error: {}
};

export default function reducer(state = initialState, action = {}) {

  switch (action.type) {
    case UPDATE:
      return {
        ...state,
        updating: true
      };
    case UPDATE_SUCCESS:
      return {
        ...state,
        updating: false
      };
    case UPDATE_FAIL:
      return {
        ...state,
        updating: false,
        error: action.error
      };
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        data: action.result.body,
        loading: false
      };
    case LOAD_FAIL:
      return {
        ...state,
        error: action.error,
        loading: false
      };
    case EDIT:
      return {
        ...state,
        editing: true
      };
    case EDIT_CANCEL:
      return {
        ...state,
        editing: false
      }
    case CLEAR_STATE:
      return initialState;
    default:
      return state;
  }
}

export function update(data) {
  return {
    types: [UPDATE, UPDATE_SUCCESS, UPDATE_FAIL],
    promise: (client, token, userId) => client.put(`users/${userId}`,
      {
        token: token,
        data: {
          ...data,
          CountryId: data.country
        }
      })
  };
}

export function load() {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token, userId) => client.get(`users/${userId}`)
  }
}

export function edit() {
  return {
    type: EDIT
  }
}

export function editCancel() {
  return {
    type: EDIT_CANCEL
  }
}
