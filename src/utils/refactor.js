export const refactorBooks = (books) => {
  return books.map((book) => {
    return {value: book.id, label: book.name}
  })
};

export const refactorCountries = (countries) => {
  return countries.map((country) => {
    return {value: country.id, label: country.name}
  })
}
