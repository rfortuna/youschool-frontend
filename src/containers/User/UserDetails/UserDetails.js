/**
 * Created by urbanmarovt on 16/04/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import {reduxForm, getValues} from 'redux-form';
import * as routerActions from 'redux-router';
import {Link} from 'react-router';
import {bindActionCreators} from 'redux';
import cookie from 'react-cookie';
import {toastr} from 'react-redux-toastr';
import {Panel, FormControls,FormControl, Button,
  FormGroup, ControlLabel, Row, Col, HelpBlock} from 'react-bootstrap';
import {CustomSelect} from 'components';

import {load as loadProfile, isLoaded as isProfileLoaded} from 'redux/modules/global/profile';
import { validateEmail, validateMobilePhone, isInteger } from 'utils/validation';
import {translateFormErrors} from 'utils/localization';
import {verifyEmailRequest} from 'redux/modules/utils/verifyEmail';
import {refactorCountries} from 'utils/refactor';
import {edit, editCancel, update as updateUser} from 'redux/modules/models/users/details';

import multiStrings from './strings';

const validate = (values) => {

  const errors = {};

  if (!values.firstName) {
    errors.firstName = 'required';
  }

  if (!values.lastName) {
    errors.lastName = 'required';
  }

  if (!values.email) {
    errors.email = 'required';
  }
  else if (!validateEmail(values.email)) {
    errors.email = 'invalidEmail';
    //Invalid email address.
  }

  if (values.mobileNumber && !validateMobilePhone(values.mobileNumber)) {
    errors.mobileNumber = 'invalidMobileNumber'
  }

  if (values.currentGrade && !isInteger(values.currentGrade)) {
    errors.currentGrade = 'invalidInteger'
  }

  if (values.targetGrade && !isInteger(values.targetGrade)) {
    errors.targetGrade = 'invalidInteger'
  }

  return errors;

};

function fetchDataDeferred(getState, dispatch) {
  const promises = [];

  if (!isProfileLoaded(getState())) {
    promises.push(dispatch(loadProfile()));
  }

  return Promise.all(promises);
}

function reformatPrefillData(initialValues) {
  const formattedData = {
    ...initialValues,
    country: initialValues.Country ? initialValues.Country.id : null
  };
  return formattedData;
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    user: state.global.profile,
    editing: state.models.users.details.editing,
    chosenLanguage: state.global.language.chosenLanguage,
    countries: state.models.countries.list.data,
    userForm: state.form.UpdateUserForm
  }),
  dispatch => bindActionCreators({...routerActions, edit, editCancel,
    updateUser, verifyEmailRequest, loadProfile}, dispatch)
)
@reduxForm({
  form: 'UpdateUserForm',
  fields: ['firstName', 'lastName', 'city', 'country', 'mobileNumber', 'school', 'schoolClass', 'currentGrade', 'targetGrade'],
  validate: validate
},
state => ({
  initialValues: reformatPrefillData(state.global.profile)
}))
export default class UserDetails extends Component {

  static propTypes = {
    details: PropTypes.object,
    chosenLanguage: PropTypes.string
  };

  handleVerifyAccount = () => {
    const {verifyEmailRequest, chosenLanguage} = this.props;
    const strings = multiStrings[chosenLanguage];

    verifyEmailRequest().then(() => {
      toastr.success(strings.toastrs.verifyEmailSuccess);
    });
  };

  handleUpdateUser = () => {
    const {userForm, updateUser, editCancel, loadProfile, chosenLanguage} = this.props;

    const data = getValues(userForm);

    const strings = multiStrings[chosenLanguage];

    updateUser(data).then((res) => {
      if (res.error) {
        toastr.error(res.error);
      } else {
        toastr.success(strings.toastrs.updateAccountSuccess);
        editCancel();
        loadProfile();
      }
    });
  }

  render() {
    const {user, chosenLanguage, errors,
      fields: {firstName, lastName, password, passwordCheck,
        city, country, mobileNumber, school, schoolClass, currentGrade,
        targetGrade}, editing, edit, editCancel, countries} = this.props;

    const strings = multiStrings[chosenLanguage];
    const translatedErrors = translateFormErrors(errors, strings.errors);

    return (
      <div className="padding-top-container pb96 container user-details">
        <div>
          <Row className="pt32 pb32">
            <Col xs={12} sm={6}>
              <Link to="/pricing"><Button className="button-primary pt8 pb8 pl24 pr24 pull-right">{strings.actions.buyCredits}</Button></Link>
              <h6>{strings.numberOfCredits}: <strong>{user.credits}</strong></h6>
            </Col>
            <Col xs={12} sm={6}>
              {(user.verified &&
                <h6><strong>{strings.accountIsVerified}</strong></h6>) ||
                <div>
                  <Button className="button-primary pt8 pb8 pl24 pr24 pull-right" onClick={this.handleVerifyAccount}>{strings.actions.verifyEmail}</Button>
                  <h6><strong>{strings.accountIsNotVerified}</strong></h6>
                </div>
              }
            </Col>
          </Row>
          <Row>
            <Col xs={12} sm={6}>
              <FormGroup validationState={ errors.firstName && firstName.touched ? 'error' : null}>
                <ControlLabel>{strings.firstName}</ControlLabel>
                {(editing &&
                  <div>
                    <FormControl type="text" placeholder={strings.placeholders.firstName} {...firstName} />
                    {firstName.touched && errors.firstName && <HelpBlock>{translatedErrors.firstName}</HelpBlock>}
                  </div>
                ) ||
                <FormControl.Static>
                  {user.firstName}
                </FormControl.Static>}
              </FormGroup>
            </Col>
            <Col xs={12} sm={6}>
              <FormGroup validationState={ errors.lastName && lastName.touched ? 'error' : null}>
                <ControlLabel>{strings.lastName}</ControlLabel>
                {(editing &&
                  <div>
                    <FormControl type="text" placeholder={strings.placeholders.lastName} {...lastName} />
                    {lastName.touched && errors.lastName && <HelpBlock>{translatedErrors.lastName}</HelpBlock>}
                  </div>
                ) ||
                  <FormControl.Static>
                    {user.lastName}
                  </FormControl.Static>
                }
              </FormGroup>
            </Col>
            <Col xs={12} sm={6}>
              <FormGroup validationState={ errors.email && email.touched ? 'error' : null}>
                <ControlLabel>{strings.email}</ControlLabel>
                <FormControl.Static>
                  {user.email}
                </FormControl.Static>
              </FormGroup>
            </Col>
            <Col xs={12} sm={6}>
              <FormGroup validationState={ errors.mobileNumber && mobileNumber.touched ? 'error' : null}>
                <ControlLabel>{strings.mobileNumber}</ControlLabel>
                  {(editing &&
                    <div>
                      <FormControl type="text" placeholder={strings.placeholders.mobileNumber} {...mobileNumber} />
                      {mobileNumber.touched && errors.mobileNumber && <HelpBlock>{translatedErrors.mobileNumber}</HelpBlock>}
                    </div>
                  ) ||
                  <FormControl.Static>
                    {user.mobileNumber}
                  </FormControl.Static>
                  }
              </FormGroup>
            </Col>
            <Col xs={12} sm={6}>
              <FormGroup >
                <ControlLabel>{strings.school}</ControlLabel>
                  {(editing &&
                    <FormControl type="text" placeholder={strings.placeholders.school} {...school} />
                  ) ||
                  <FormControl.Static>
                    {user.school}
                  </FormControl.Static>
                  }
              </FormGroup>
            </Col>
            <Col xs={12} sm={6}>
              <FormGroup>
                <ControlLabel>{strings.schoolClass}</ControlLabel>
                  {(editing &&
                    <FormControl type="text" placeholder={strings.placeholders.schoolClass} {...schoolClass} />
                  ) ||
                  <FormControl.Static>
                    {user.schoolClass}
                  </FormControl.Static>
                }
              </FormGroup>
            </Col>
            <Col xs={12} sm={6}>
              <FormGroup validationState={ errors.currentGrade && currentGrade.touched ? 'error' : null}>
                <ControlLabel>{strings.currentGrade}</ControlLabel>
                  {(editing &&
                    <div>
                      <FormControl type="text" placeholder={strings.placeholders.currentGrade} {...currentGrade} />
                      {currentGrade.touched && errors.currentGrade && <HelpBlock>{translatedErrors.currentGrade}</HelpBlock>}
                    </div>
                  ) ||
                  <FormControl.Static>
                    {user.currentGrade}
                  </FormControl.Static>
                  }
              </FormGroup>
            </Col>
            <Col xs={12} sm={6}>
              <FormGroup validationState={ errors.targetGrade && targetGrade.touched ? 'error' : null}>
                <ControlLabel>{strings.targetGrade}</ControlLabel>
                  {(editing &&
                    <div>
                      <FormControl type="text" placeholder={strings.placeholders.targetGrade} {...targetGrade} />
                      {targetGrade.touched && errors.targetGrade && <HelpBlock>{translatedErrors.targetGrade}</HelpBlock>}
                    </div>
                  ) ||
                  <FormControl.Static>
                    {user.targetGrade}
                  </FormControl.Static>
                  }
              </FormGroup>
            </Col>
            <Col xs={12} sm={6}>
              <FormGroup>
                <ControlLabel>{strings.city}</ControlLabel>
                  {(editing &&
                      <FormControl type="text" placeholder={strings.placeholders.city} {...city} />
                  ) ||
                  <FormControl.Static>
                    {user.city}
                  </FormControl.Static>
                  }
              </FormGroup>
            </Col>
            <Col xs={12} sm={6}>
              <FormGroup>
                <ControlLabel>{strings.country}</ControlLabel>
                  {(editing &&
                    <CustomSelect {...country} data={refactorCountries(countries)} placeholder={strings.placeholders.country}/>
                  ) ||
                  <FormControl.Static>
                    {user && user.Country && user.Country.name}
                  </FormControl.Static>
                  }
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              {!editing &&
                <div>
                  <Button className="button-primary pt8 pb8 pl24 pr24" onClick={edit}>{strings.actions.edit}</Button>
                </div>
                ||
                <div>
                  <Button className="button-primary pt8 pb8 pl24 pr24 mr16" onClick={this.handleUpdateUser}>{strings.actions.update}</Button>
                  <Button className="button-dangerous pt8 pb8 pl24 pr24" onClick={editCancel}>{strings.actions.cancel}</Button>
                </div>
              }
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}
