const LOAD = 'youschool/countries/LOAD';
const LOAD_SUCCESS = 'youschool/countries/LOAD_SUCCESS';
const LOAD_FAIL = 'youschool/countries/LOAD_FAIL';

const initialState = {
  loaded: false,
  error: {},
  data: []
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        data: [],
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.body,
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: [],
        error: action.error
      };
    default:
      return state;
  }
}

// put globalState as parameters
export function load() {

  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token) => client.get('/countries', {})
  };

}
