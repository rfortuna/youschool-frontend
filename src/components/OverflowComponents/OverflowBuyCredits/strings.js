/**
 * Created by urbanmarovt on 24/04/16.
 */

export default {
  en: {
    buyCredits: 'Buy credits',
    hint: "Unfortunately, Currently you don't have enough credits to purchase this video.",
    actions: {
      buyCredits: 'Buy credits',
      cancel: 'Cancel'
    }
  },
  sv: {
    buyCredits: 'Buy credits',
    hint: "Unfortunately, Currently you don't have enough credits to purchase this video.",
    actions: {
      buyCredits: 'Buy credits',
      cancel: 'Cancel'
    }
  }
};
