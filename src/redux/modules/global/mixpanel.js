/**
 * Created by rokfortuna on 4/16/16.
 */

import config from './../../../config';

const INIT = 'youschool/global/mixpanel/INIT';
const TRACK_EVENT = 'youschool/global/mixpanel/TRACK_EVENT';
const IDENTIFY = 'youschool/global/mixpanel/IDENTIFY';
const SET_PROFILE = 'youschool/global/mixpanel/SET_PROFILE';

const initialState = {
  mixpanel: null
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case INIT:
      if (__CLIENT__) {
        console.log(`MIXPANEL INIT`);
        let mixpanel =  require('mixpanel-browser');
        mixpanel.init(config.mixpanelToken);
        return {
          ...state,
          mixpanel: mixpanel
        }
      }
      return initialState;
    case TRACK_EVENT:
      if (__CLIENT__) {
        console.log(`MIXPANEL EVENT: ${action.data.name}`);
        state.mixpanel.track(action.data.name, action.data.attributes);
      }
      return state;
    case IDENTIFY:
      if (__CLIENT__) {
        console.log(`MIXPANEL IDENTIFY: ${action.data.userId}`);
        state.mixpanel.identify(action.data.userId);
      }
      return state;
    case SET_PROFILE:
      if (__CLIENT__) {
        console.log('SET PROFILE');
        state.mixpanel.people.set(action.data.userAttributes);
        state.mixpanel.identify(action.data.userId);
      }
      return state;
    default:
      return state;
  }
}

export function init() {
  return {
    type: INIT
  };
}

export function trackEvent(name, attributes) {
  return {
    type: TRACK_EVENT,
    data: {name: name, attributes: attributes}
  };
}

export function identify(userId) {
  return {
    type: IDENTIFY,
    data: {userId: userId}
  };
}

export function setProfile(userId, userAttributes) {
  return {
    type: SET_PROFILE,
    data: {userId: userId, userAttributes: userAttributes}
  };
}


