/**
 * Created by urbanmarovt on 26/04/16.
 */

const BUY_CREDITS_REQUEST = 'youschool/authentication/BUY_CREDITS_REQUEST';
const BUY_CREDITS_REQUEST_SUCCESS = 'youschool/authentication/BUY_CREDITS_REQUEST_SUCCESS';
const BUY_CREDITS_REQUEST_FAIL = 'youschool/authentication/BUY_CREDITS_REQUEST_FAIL';

const initialState = {
  error: {},
  requestingBuyCredits: false
};

export default function reducer(state = initialState, action = {}) {

  switch (action.type) {
    case BUY_CREDITS_REQUEST:
      return {
        ...state,
        requestingBuyCredits: true
      };
    case BUY_CREDITS_REQUEST_SUCCESS:

      return {
        ...state,
        requestingBuyCredits: false
      };
    case BUY_CREDITS_REQUEST_FAIL:
      return {
        ...state,
        requestingBuyCredits: false,
        error: action.error
      };
    default:
      return state;
  }
}

export function requestBuyCredits(mobileNumber, planId) {
  return {
    types: [BUY_CREDITS_REQUEST, BUY_CREDITS_REQUEST_SUCCESS, BUY_CREDITS_REQUEST_FAIL],
    promise: (client, token) => client.post(`/payment/request`, {
      data: {
        mobileNumber: mobileNumber,
        planId: planId
      },
      token: token
    })
  };
}


